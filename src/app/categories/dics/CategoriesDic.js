(function () { 'use strict';

categoriesModule.factory("Categories.CategoriesDic", CategoriesDicFactory);

CategoriesDicFactory.$inject = ["Commons.BaseDic"];
function CategoriesDicFactory(BaseDic)
{
    class CategoriesDic extends BaseDic
    {
        constructor() {
            super("categories");

            this.new_entity = {
                id: 0,
                id_editable: "",
                descripcio: "",
                inits: false,
                postinits: false,
                limpieza: false,
                estado: false,
                solucion: false,
                initsJP: false,
                entradaJP: false,
                limpiezaJP: false,
                comprobaciones: false,
            };
        }
    }

    return new CategoriesDic();
}

})();