(function (States) { 'use strict';

States.categories =

    {   name     : "categories",
        parent   : "root",
        views    : {
            "mainOutlet": {
                template: "<CATEGORIES></CATEGORIES>"
        }},
        children : [

            {   name     : "category",
                abstract : "categoryForm", // true
                params   : {"id": {value: null}}, // allows us to 1) working without URLs, and 2) not having to define a URL for this abstract state if we are working with URLs :) We just borrow the {id} URL param from the leaf child state where the actual final URL is defined :)
                views    : {
                    "categoryOutlet": {
                        template: "<CATEGORY></CATEGORY>"
                }},
                children : [

                    {   name  : "categoryForm",
                        views : {
                            "categoryContentOutlet": {
                                template: "<CATEGORY-FORM></CATEGORY-FORM>"
                    }}},

                ]
            }
        ]
    }
;

})(APP.States);
