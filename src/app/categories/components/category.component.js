(function (ng) { 'use strict';

categoriesModule.component("category", {
    controller: CategoryCtrl, controllerAs: "VM",
    templateUrl: "categories/components/category.tpl.html"
});

CategoryCtrl.$inject = [
"Categories.CategoriesDic",
"Commons.UiStateManager",
"Commons.Utils",
"$scope",
"$state",
"$translate"];
function CategoryCtrl(CategoriesDic, UiStateManager, Utils, $scope,  $state, $translate)
{
    var VM = this;
    ng.extend(VM, {
        /**
         * Model
         */
        category: null,
        isNew: false,
        isDirty: false,
        mood: null,

        /**
         * Actions
         */
        getField: getField,
        saveChanges: saveChanges,
        revertChanges: revertChanges,
        remove: remove,
    });

    componentDidMount();

    /***************************************************************************
     * Implementation
     **************************************************************************/

    function componentDidMount() {
        refresh();

        $scope.$on("categorySelected", onCategorySelected);
        $scope.$on("categoryModified", onCategoryModified);
    }

    function getField(field) {
        if (VM.category) {
            return CategoriesDic.getWipValueOrFallback(VM.category, field);
        }
        return "";
    }

    function saveChanges() {
        $scope.$broadcast("doCategorySave");
    }

    function revertChanges() {
        $scope.$broadcast("doCategoryRevert");
    }

    function remove() {
        if (confirm($translate.instant("CONFIRM_REMOVE"))) {
            $scope.$emit("doShowLoader", true);
            CategoriesDic.remove(VM.category, onRemovedSuccess, onRemovedError);
        }
    }

    function onRemovedSuccess() {
        $state.go("categories").then(function () {
            alert($translate.instant("REMOVED_ENTITY")+"\n\n:)");
            $scope.$emit("doShowLoader", false);
        });
    }

    function onRemovedError(response) {
        if (ng.isDefined(response.exception)) {
            alert("Server returned an Exception:\n"+response.exception);
        } else {
            alert("Server gave an unexpected answer:\n"+JSON.stringify(response));
        }
        $scope.$emit("doShowLoader", false);
    }

    function refresh() {
        VM.category = UiStateManager.get("categories.category");
        if (VM.category) {
            VM.isNew = (VM.category.id === 0);

            refresh2();
        }
    }

    function refresh2() {
        VM.isDirty = CategoriesDic.hasChanges(VM.category, false);
        refreshMood();
    }

    function refreshMood() {
        VM.mood = "neutral";
        if (VM.isNew) {
            VM.mood = "primary";
        } else if (VM.isDirty) {
            VM.mood = "warning";
        }
    }

    function onCategorySelected(e, data) {
        refresh();
    }

    function onCategoryModified(e) {
        refresh2();
    }
}


})(angular);