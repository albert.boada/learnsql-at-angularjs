(function (ng) { 'use strict';

categoriesModule.component("categoryForm", {
    controller  : CategoryFormCtrl, controllerAs: "VM",
    templateUrl : "categories/components/form.tpl.html"
});

CategoryFormCtrl.$inject = [
"Categories.CategoriesDic",
"$state",
"$scope",
"Commons.UiStateManager",
"Commons.Utils",
"$translate",
"$rootScope"];
function CategoryFormCtrl(CategoriesDic, $state, $scope, UiStateManager, Utils, $translate, $rootScope)
{
    var VM = this;
    ng.extend(VM, {
        /**
         * Model
         */
        category: null,
    });

    var _category;

    componentDidMount();

    /***************************************************************************
     * Implementation
     **************************************************************************/

    function componentDidMount() {
        refresh();

        $scope.$on("categorySelected", onCategorySelected);
        $scope.$on("doCategorySave", onDoSave);
        $scope.$on("doCategoryRevert", onDoRevert);
        $scope.$watch(function () { return VM.category; }, onCategoryModified, true); // true!!
    }

    function refresh() {
        _category = UiStateManager.get("categories.category");
        if (_category) {
            VM.category = CategoriesDic.getWip(_category);
        }
    }

    function onCategorySelected(e, data) {
        refresh();
    }

    function onCategoryModified(_new, _old) {
        if (_new && _old && _new.id == _old.id) {
            CategoriesDic.checkForChanges(_category);

            $scope.$emit("categoryModified");
        }
    }

    var _old_category_id;

    function onDoSave() {
         if (confirm($translate.instant("CONFIRM_SAVE"))) {
            $scope.$emit("doShowLoader", true);
            if (_category.id) {
                _old_category_id = _category.id;
                CategoriesDic.saveChanges(_category, onSaveSuccess, onSaveError);
            } else {
                CategoriesDic.saveNew(onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(response) {
            alert($translate.instant("SAVE_SUCCESS")+"\n\n:)");
            if (_category.id === 0 || _old_category_id != response.id) {
                $scope.$emit("doGoToCategory", response.id);
            }
            else {
                $rootScope.$broadcast("categorySelected");
            }

            $scope.$emit("doShowLoader", false);
        }

        function onSaveError(response) {
            if (ng.isDefined(response.validation)) {
                var error      = response.validation[0];
                var path       = response.validation[1];
                var field      = response.validation[2];
                var error_vars = response.validation[3];

                Utils.alertValidationError(error, error_vars, path, field);
            }
            else {
                Utils.alertServerError(response);
            }
            $scope.$emit("doShowLoader", false);
        }
    }


    function onDoRevert() {
        if (confirm($translate.instant("CONFIRM_REVERT"))) {
            CategoriesDic.revertChanges(_category);
            $rootScope.$broadcast("categorySelected");
        }
    }
}

})(angular);
