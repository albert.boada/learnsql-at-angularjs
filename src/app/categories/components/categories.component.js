(function (ng) { 'use strict';

categoriesModule.component("categories", {
    controller: CategoriesCtrl, controllerAs: "VM",
    templateUrl: "categories/components/categories.tpl.html"
});

CategoriesCtrl.$inject = [
"Categories.CategoriesDic",
"Commons.UiStateManager",
"Commons.Utils",
"$scope",
"$state",
"$stateParams"];
function CategoriesCtrl(CategoriesDic, UiStateManager, Utils, $scope, $state, $stateParams)
{
    var VM = this;
    ng.extend(VM, {
        /**
         * Model
         */
        categories: [],
        shouldShowSelector: true,
        num_total: 0,

        /**
         * Actions
         */
        pick: pick,
        addNew: addNew,
        getField: getField,
        isCurrent: isCurrent,
        isDirty: isDirty,
        toggleSelector: toggleSelector,
    });

    var _current;

    componentDidMount();

    /***************************************************************************
     * Implementation
     **************************************************************************/

    function componentDidMount() {
        VM.categories = CategoriesDic.getAll();

        refresh();

        $scope.$on("doGoToCategory", onDoGoToCategory);

        $scope.$watch(function () { return $stateParams.id; }, onUrlIdChanged);
    }

    function refresh() {
        var cat;
        if (ng.isDefined($stateParams.id)) {
            if (Utils.isCreateRouteKeyword($stateParams.id)) {
                cat = CategoriesDic.new_entity;
            } else {
                cat = CategoriesDic.getInstance($stateParams.id);
            }

            if (!cat) {
                $state.go("categories").then(function () { alert("cat not found"); });
                return;
            }
        }
        setCurrent(cat);

        VM.num_total = VM.categories.length;
    }

    function pick(id) {
        var transitionTo = {name: "categoryForm", params: {"id": id}};

        if ($state.is("categories")) {
            $state.go(transitionTo.name, transitionTo.params);
            return;
        }

        Utils.goWithoutReload(transitionTo, "mainOutlet@root", 1);
    }

    function addNew() {
        pick("new");
    }

    function getField(category, field) {
        return CategoriesDic.getWipValueOrFallback(category, field);
    }

    function toggleSelector() {
        VM.shouldShowSelector = !VM.shouldShowSelector;
    }

    function isDirty(category) {
        return CategoriesDic.hasChanges(category, false);
    }

    function isCurrent(id) {
        return _current && _current.id == id;
    }

    function onUrlIdChanged(_new, _old) {
        if ($state.includes("categories") && _new != _old) {
            refresh();
        }
    }

    function onDoGoToCategory(e, id) {
        pick(id);
    }

    function setCurrent(category) {
        _current = category;
        broadcast(category);
    }

    function broadcast(category) {
        UiStateManager.set("categories.category", category);
        $scope.$broadcast("categorySelected");
    }
}

})(angular);