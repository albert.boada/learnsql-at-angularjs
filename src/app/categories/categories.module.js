var categoriesModule;

(function (ng) { 'use strict';

var dependencies = [];

categoriesModule = ng.module(appName+".Categories", dependencies);
categoriesModule.config(categoriesModuleConfig);
categoriesModule.run(categoriesModuleRun);

function categoriesModuleConfig() {}

categoriesModuleRun.$inject = [];
function categoriesModuleRun() {}

})(angular);