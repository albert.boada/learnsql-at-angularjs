(function (ng) { 'use strict';

schemasModule.component("schema", {
    controller: SchemaCtrl, controllerAs: "VM",
    templateUrl: "schemas/components/schema.tpl.html"
});

SchemaCtrl.$inject = [
"Schemas.SchemasDic",
"Commons.UiStateManager",
"Commons.Utils",
"$scope",
"$state",
"$translate"];
function SchemaCtrl(SchemasDic, UiStateManager, Utils, $scope,  $state, $translate)
{
    var VM = this;
    ng.extend(VM, {
        /**
         * Model
         */
        schema: null,
        isNew: false,
        isDirty: false,
        mood: null,

        /**
         * Action
         */
        getField: getField,
        saveChanges: saveChanges,
        revertChanges: revertChanges,
        remove: remove,
    });

    componentDidMount();

    /***************************************************************************
     * Implementation
     **************************************************************************/

    function componentDidMount() {
        refresh();

        $scope.$on("schemaSelected", onSchemaSelected);
        $scope.$on("schemaModified", onSchemaModified);
    }

    function getField(field) {
        if (VM.schema) {
            return SchemasDic.getWipValueOrFallback(VM.schema, field);
        }
        return "";
    }

    function saveChanges() {
        $scope.$broadcast("doSchemaSave");
    }

    function revertChanges() {
        $scope.$broadcast("doSchemaRevert");
    }

    function remove() {
        if (confirm($translate.instant("CONFIRM_REMOVE"))) {
            $scope.$emit("doShowLoader", true);
            SchemasDic.remove(VM.schema, onRemovedSuccess, onRemovedError);
        }
    }

    function onRemovedSuccess() {
        $state.go("schemas").then(function () {
            alert($translate.instant("REMOVED_ENTITY")+"\n\n:)");
            $scope.$emit("doShowLoader", false);
        });
    }

    function onRemovedError(response) {
        if (ng.isDefined(response.exception)) {
            alert("Server returned an Exception:\n"+response.exception);
        } else {
            alert("Server gave an unexpected answer:\n"+JSON.stringify(response));
        }
        $scope.$emit("doShowLoader", false);
    }

    function refresh() {
        VM.schema = UiStateManager.get("schemas.schema");
        if (VM.schema) {
            VM.isNew = (VM.schema.id === 0);

            refresh2();
        }
    }

    function refresh2() {
        VM.isDirty = SchemasDic.hasChanges(VM.schema, false);
        refreshMood();
    }

    function refreshMood() {
        VM.mood = "neutral";
        if (VM.isNew) {
            VM.mood = "primary";
        } else if (VM.isDirty) {
            VM.mood = "warning";
        }
    }

    function onSchemaSelected(e, data) {
        refresh();
    }

    function onSchemaModified(e) {
        refresh2();
    }
}


})(angular);