(function (ng) { 'use strict';

schemasModule.component("schemas", {
    controller: SchemasCtrl, controllerAs: "VM",
    templateUrl: "schemas/components/schemas.tpl.html"
});

SchemasCtrl.$inject = [
"Schemas.SchemasDic",
"Commons.UiStateManager",
"Commons.Utils",
"$scope",
"$state",
"$stateParams"];
function SchemasCtrl(SchemasDic, UiStateManager, Utils, $scope, $state, $stateParams)
{
    var VM = this;
    ng.extend(VM, {
        /**
         * Model
         */
        schemas: [],
        shouldShowSelector: true,
        num_total: 0,

        /**
         * Actions
         */
        pick: pick,
        addNew: addNew,
        getField: getField,
        isCurrent: isCurrent,
        isDirty: isDirty,
        toggleSelector: toggleSelector,
    });

    var _current;

    componentDidMount();

    /***************************************************************************
     * Implementation
     **************************************************************************/

    function componentDidMount() {
        VM.schemas = SchemasDic.getAll();

        refresh();

        $scope.$on("doGoToSchema", onDoGoToSchema);

        $scope.$watch(function () { return $stateParams.id; }, onUrlIdChanged);
    }

    function refresh() {
        var sch;
        if (ng.isDefined($stateParams.id)) {
            if (Utils.isCreateRouteKeyword($stateParams.id)) {
                sch = SchemasDic.new_entity;
            } else {
                sch = SchemasDic.getInstance($stateParams.id);
            }

            if (!sch) {
                $state.transitionTo("schemas").then(function () { alert("sch not found"); });
                return;
            }
        }
        setCurrent(sch);

        VM.num_total = VM.schemas.length;
    }

    function pick(id) {
        var transitionTo = {name: "schemaForm", params: {"id": id}};

        if ($state.is("schemas")) {
            $state.go(transitionTo.name, transitionTo.params);
            return;
        }

        Utils.goWithoutReload(transitionTo, "mainOutlet@root", 1);
    }

    function addNew() {
        pick("new");
    }

    function getField(schema, field) {
        return SchemasDic.getWipValueOrFallback(schema, field);
    }

    function toggleSelector() {
        VM.shouldShowSelector = !VM.shouldShowSelector;
    }

    function isDirty(schema) {
        return SchemasDic.hasChanges(schema, false);
    }

    function isCurrent(id) {
        return _current && _current.id == id;
    }

    function onUrlIdChanged(_new, _old) {
        if ($state.includes("schemas") && _new != _old) {
           refresh();
        }
    }

    function onDoGoToSchema(e, id) {
        pick(id);
    }

    function setCurrent(schema) {
        _current = schema;
        broadcast(schema);
    }

    function broadcast(schema) {
        UiStateManager.set("schemas.schema", schema);
        $scope.$broadcast("schemaSelected");
    }
}

})(angular);