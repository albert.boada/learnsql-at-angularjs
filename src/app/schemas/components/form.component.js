(function (ng) { 'use strict';

schemasModule.component("schemaForm", {
    controller  : SchemaFormCtrl, controllerAs: "VM",
    templateUrl : "schemas/components/form.tpl.html"
});

SchemaFormCtrl.$inject = [
"Schemas.SchemasDic",
"$state",
"$scope",
"Commons.UiStateManager",
"Commons.Utils",
"$translate",
"$rootScope"];
function SchemaFormCtrl(SchemasDic, $state, $scope, UiStateManager, Utils, $translate, $rootScope)
{
    var VM = this;
    ng.extend(VM, {
        /**
         * Model
         */
        schema: null,
    });

    var _schema;

    componentDidMount();

    /***************************************************************************
     * Implementation
     **************************************************************************/

    function componentDidMount() {
        refresh();

        $scope.$on("schemaSelected", onSchemaSelected);
        $scope.$on("doSchemaSave", onDoSave);
        $scope.$on("doSchemaRevert", onDoRevert);
        $scope.$watch(function () { return VM.schema; }, onSchemaModified, true); // true!!
    }

    function refresh() {
        _schema = UiStateManager.get("schemas.schema");
        if (_schema) {
            VM.schema = SchemasDic.getWip(_schema);
        }
    }

    function onSchemaSelected(e, data) {
        refresh();
    }

    function onSchemaModified(_new, _old) {
        if (_new && _old && _new.id == _old.id) {
            SchemasDic.checkForChanges(_schema);

            $scope.$emit("schemaModified");
        }
    }

    var _old_schema;

    function onDoSave() {
         if (confirm($translate.instant("CONFIRM_SAVE"))) {
            $scope.$emit("doShowLoader", true);
            if (_schema.id) {
                _old_schema = _schema.id;
                SchemasDic.saveChanges(_schema, onSaveSuccess, onSaveError);
            } else {
                SchemasDic.saveNew(onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(response) {
            alert($translate.instant("SAVE_SUCCESS")+"\n\n:)");
            if (_schema.id === 0 || _old_schema != response.id) {
                $scope.$emit("doGoToSchema", response.id);
            }
            else {
                $rootScope.$broadcast("schemaSelected");
            }

            $scope.$emit("doShowLoader", false);
        }

        function onSaveError(response) {
            if (ng.isDefined(response.validation)) {
                var error      = response.validation[0];
                var path       = response.validation[1];
                var field      = response.validation[2];
                var error_vars = response.validation[3];

                Utils.alertValidationError(error, error_vars, path, field);
            }
            else {
                Utils.alertServerError(response);
            }
            $scope.$emit("doShowLoader", false);
        }
    }


    function onDoRevert() {
        if (confirm($translate.instant("CONFIRM_REVERT"))) {
            SchemasDic.revertChanges(_schema);
            $rootScope.$broadcast("schemaSelected");
        }
    }
}

})(angular);
