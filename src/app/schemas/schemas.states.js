(function (States) { 'use strict';

States.schemas =

    {   name     : "schemas",
        parent   : "root",
        views    : {
            "mainOutlet": {
                template: "<SCHEMAS></SCHEMAS>"
        }},
        children : [

            {   name     : "schema",
                abstract : "schemaForm", // true
                params   : {"id": {value: null}}, // allows us to 1) working without URLs, and 2) not having to define a URL for this abstract state if we are working with URLs :) We just borrow the {id} URL param from the leaf child state where the actual final URL is defined :)
                views    : {
                    "schemaOutlet": {
                        template: "<SCHEMA></SCHEMA>"
                }},
                children : [

                    {   name  : "schemaForm",
                        views : {
                            "schemaContentOutlet": {
                                template: "<SCHEMA-FORM></SCHEMA-FORM>"
                    }}},

                ]
            }
        ]
    }
;

})(APP.States);
