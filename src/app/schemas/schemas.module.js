var schemasModule;

(function (ng) { 'use strict';

var dependencies = [];

schemasModule = ng.module(appName+".Schemas", dependencies);
schemasModule.config(schemasModuleConfig);
schemasModule.run(schemasModuleRun);

function schemasModuleConfig() {}

schemasModuleRun.$inject = [];
function schemasModuleRun() {}

})(angular);