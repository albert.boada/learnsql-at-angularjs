(function () { 'use strict';

schemasModule.factory("Schemas.SchemasDic", SchemasDicFactory);

SchemasDicFactory.$inject = ["Commons.BaseDic"];
function SchemasDicFactory(BaseDic)
{
    class SchemasDic extends BaseDic
    {
        constructor() {
            super("schemas");

            this.new_entity = {
                id: 0,
                nombre: "",
                descripcion: "",
            };
        }
    }

    return new SchemasDic();
}

})();