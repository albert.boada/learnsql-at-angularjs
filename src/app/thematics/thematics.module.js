var thematicsModule;

(function (ng) { 'use strict';

var dependencies = [];

thematicsModule = ng.module(appName+".Thematics", dependencies);
thematicsModule.config(thematicsModuleConfig);
thematicsModule.run(thematicsModuleRun);

function thematicsModuleConfig() {}

thematicsModuleRun.$inject = [];
function thematicsModuleRun() {}

})(angular);