(function () { 'use strict';

thematicsModule.factory("Thematics.ThematicsDic", ThematicsDicFactory);

ThematicsDicFactory.$inject = ["Commons.BaseDic"];
function ThematicsDicFactory(BaseDic)
{
    class ThematicsDic extends BaseDic
    {
        constructor() {
            super("thematics");

            this.new_entity = {
                id: 0,
                nombre: "",
                descripcion: "",
                categoriasIds: [],
            };
        }

        getByCategoryId(category_id) {
            var thematics = [];
            var all = this.getAll();

            var thematic;
            for (var i in all) {
                thematic = all[i];
                if (thematic.categoriasIds.indexOf(category_id) != -1) {
                    thematics.push(thematic);
                }
            }

            return thematics;
        }
    }

    return new ThematicsDic();
}

})();