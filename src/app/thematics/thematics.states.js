(function (States) { 'use strict';

States.thematics =

    {   name     : "thematics",
        parent   : "root",
        views    : {
            "mainOutlet": {
                template: "<THEMATICS></THEMATICS>"
        }},
        children : [

            {   name     : "thematic",
                abstract : "thematicForm", // true
                params   : {"id": {value: null}}, // allows us to 1) working without URLs, and 2) not having to define a URL for this abstract state if we are working with URLs :) We just borrow the {id} URL param from the leaf child state where the actual final URL is defined :)
                views    : {
                    "thematicOutlet": {
                        template: "<THEMATIC></THEMATIC>"
                }},
                children : [

                    {   name  : "thematicForm",
                        views : {
                            "thematicContentOutlet": {
                                template: "<THEMATIC-FORM></THEMATIC-FORM>"
                    }}},

                ]
            }
        ]
    }
;

})(APP.States);
