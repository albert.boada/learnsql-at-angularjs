(function (ng) { 'use strict';

thematicsModule.component("thematic", {
    controller: ThematicCtrl, controllerAs: "VM",
    templateUrl: "thematics/components/thematic.tpl.html"
});

ThematicCtrl.$inject = [
"Thematics.ThematicsDic",
"Commons.UiStateManager",
"Commons.Utils",
"$scope",
"$state",
"$translate"];
function ThematicCtrl(ThematicsDic, UiStateManager, Utils, $scope,  $state, $translate)
{
    var VM = this;
    ng.extend(VM, {
        /**
         * Model
         */
        thematic: null,
        isNew: false,
        isDirty: false,
        mood: null,

        /**
         * Actions
         */
        getField: getField,
        saveChanges: saveChanges,
        revertChanges: revertChanges,
        remove: remove,
    });

    componentDidMount();

    /***************************************************************************
     * Implementation
     **************************************************************************/

    function componentDidMount() {
        refresh();

        $scope.$on("thematicSelected", onThematicSelected);
        $scope.$on("thematicModified", onThematicModified);
    }

    function getField(field) {
        if (VM.thematic) {
            return ThematicsDic.getWipValueOrFallback(VM.thematic, field);
        }
        return "";
    }

    function saveChanges() {
        $scope.$broadcast("doThematicSave");
    }

    function revertChanges() {
        $scope.$broadcast("doThematicRevert");
    }

    function remove() {
        if (confirm($translate.instant("CONFIRM_REMOVE"))) {
            $scope.$emit("doShowLoader", true);
            ThematicsDic.remove(VM.thematic, onRemovedSuccess, onRemovedError);
        }
    }

    function onRemovedSuccess() {
        $state.go("thematics").then(function () {
            alert($translate.instant("REMOVED_ENTITY")+"\n\n:)");
            $scope.$emit("doShowLoader", false);
        });
    }

    function onRemovedError(response) {
        if (ng.isDefined(response.exception)) {
            alert("Server returned an Exception:\n"+response.exception);
        } else {
            alert("Server gave an unexpected answer:\n"+JSON.stringify(response));
        }
        $scope.$emit("doShowLoader", false);
    }

    function refresh() {
        VM.thematic = UiStateManager.get("thematics.thematic");
        if (VM.thematic) {
            VM.isNew = (VM.thematic.id === 0);

            refresh2();
        }
    }

    function refresh2() {
        VM.isDirty = ThematicsDic.hasChanges(VM.thematic, false);
        refreshMood();
    }

    function refreshMood() {
        VM.mood = "neutral";
        if (VM.isNew) {
            VM.mood = "primary";
        } else if (VM.isDirty) {
            VM.mood = "warning";
        }
    }

    function onThematicSelected(e, data) {
        refresh();
    }

    function onThematicModified(e) {
        refresh2();
    }
}


})(angular);