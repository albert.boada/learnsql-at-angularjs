(function (ng) { 'use strict';

thematicsModule.component("thematicForm", {
    controller  : ThematicFormCtrl, controllerAs: "VM",
    templateUrl : "thematics/components/form.tpl.html"
});

ThematicFormCtrl.$inject = [
"Thematics.ThematicsDic",
"Categories.CategoriesDic",
"$state",
"$scope",
"Commons.UiStateManager",
"$translate",
"$rootScope"];
function ThematicFormCtrl(ThematicsDic, CategoriesDic, $state, $scope, UiStateManager, $translate, $rootScope)
{
    var VM = this;
    ng.extend(VM, {
        /**
         * Model
         */
        thematic: null,
        categories: [],
    });

    var _thematic;

    componentDidMount();

    /***************************************************************************
     * Implementation
     **************************************************************************/

    function componentDidMount() {
        refresh();

        VM.categories = CategoriesDic.getAll();

        $scope.$on("thematicSelected", onThematicSelected);
        $scope.$on("doThematicSave", onDoSave);
        $scope.$on("doThematicRevert", onDoRevert);
        $scope.$watch(function () { return VM.thematic; }, onThematicModified, true); // true!!
    }

    function refresh() {
        _thematic = UiStateManager.get("thematics.thematic");
        if (_thematic) {
            VM.thematic = ThematicsDic.getWip(_thematic);
        }
    }

    function onThematicSelected(e, data) {
        refresh();
    }

    function onThematicModified(_new, _old) {
        if (_new && _old && _new.id == _old.id) {
            ThematicsDic.checkForChanges(_thematic);

            $scope.$emit("thematicModified");
        }
    }

    var _old_thematic_id;

    function onDoSave() {
         if (confirm($translate.instant("CONFIRM_SAVE"))) {
            $scope.$emit("doShowLoader", true);
            if (_thematic.id) {
                _old_thematic_id = _thematic.id;
                ThematicsDic.saveChanges(_thematic, onSaveSuccess, onSaveError);
            } else {
                ThematicsDic.saveNew(onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(response) {
            alert($translate.instant("SAVE_SUCCESS")+"\n\n:)");
            if (_thematic.id === 0 || _old_thematic_id != response.id) {
                $scope.$emit("doGoToThematic", response.id);
            }
            else {
                $rootScope.$broadcast("thematicSelected");
            }

            $scope.$emit("doShowLoader", false);

        }

        function onSaveError(response) {
            if (ng.isDefined(response.validation)) {
                var error      = response.validation[0];
                var path       = response.validation[1];
                var field      = response.validation[2];
                var error_vars = response.validation[3];

                Utils.alertValidationError(error, error_vars, path, field);
            }
            else {
                Utils.alertServerError(response);
            }
            $scope.$emit("doShowLoader", false);
        }
    }


    function onDoRevert() {
        if (confirm($translate.instant("CONFIRM_REVERT"))) {
            ThematicsDic.revertChanges(_thematic);
            $rootScope.$broadcast("thematicSelected");
        }
    }
}

})(angular);
