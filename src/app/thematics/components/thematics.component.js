(function (ng) { 'use strict';

thematicsModule.component("thematics", {
    controller: ThematicsCtrl, controllerAs: "VM",
    templateUrl: "thematics/components/thematics.tpl.html"
});

ThematicsCtrl.$inject = [
"Thematics.ThematicsDic",
"Commons.UiStateManager",
"Commons.Utils",
"$scope",
"$state",
"$stateParams"];
function ThematicsCtrl(ThematicsDic, UiStateManager, Utils, $scope, $state, $stateParams)
{
    var VM = this;
    ng.extend(VM, {
        /**
         * Model
         */
        thematics: [],
        shouldShowSelector: true,
        num_total: 0,

        /**
         * Actions
         */
        pick: pick,
        addNew: addNew,
        getField: getField,
        isCurrent: isCurrent,
        isDirty: isDirty,
        toggleSelector: toggleSelector,
    });

    var _current;

    componentDidMount();

    /***************************************************************************
     * Implementation
     **************************************************************************/

    function componentDidMount() {
        VM.thematics = ThematicsDic.getAll();

        refresh();

        $scope.$on("doGoToThematic", onDoGoToThematic);

        $scope.$watch(function () { return $stateParams.id; }, onUrlIdChanged);
    }

    function refresh() {
        var them;
        if (ng.isDefined($stateParams.id)) {
            if (Utils.isCreateRouteKeyword($stateParams.id)) {
                them = ThematicsDic.new_entity;
            } else {
                them = ThematicsDic.getInstance($stateParams.id);
            }

            if (!them) {
                $state.transitionTo("thematics").then(function () { alert("them not found"); });
                return;
            }
        }
        setCurrent(them);

        VM.num_total = VM.thematics.length;
    }

    function pick(id) {
        var transitionTo = {name: "thematicForm", params: {"id": id}};

        if ($state.is("thematics")) {
            $state.go(transitionTo.name, transitionTo.params);
            return;
        }

        Utils.goWithoutReload(transitionTo, "mainOutlet@root", 1);
    }

    function addNew() {
        pick("new");
    }

    function getField(thematic, field) {
        return ThematicsDic.getWipValueOrFallback(thematic, field);
    }

    function toggleSelector() {
        VM.shouldShowSelector = !VM.shouldShowSelector;
    }

    function isDirty(thematic) {
        return ThematicsDic.hasChanges(thematic, false);
    }

    function isCurrent(id) {
        return _current && _current.id == id;
    }

    function onUrlIdChanged(_new, _old) {
        if ($state.includes("thematics") && _new != _old) {
           refresh();
        }
    }

    function onDoGoToThematic(e, id) {
        pick(id);
    }

    function setCurrent(thematic) {
        _current = thematic;
        broadcast(thematic);
    }

    function broadcast(thematic) {
        UiStateManager.set("thematics.thematic", thematic);
        $scope.$broadcast("thematicSelected");
    }
}

})(angular);