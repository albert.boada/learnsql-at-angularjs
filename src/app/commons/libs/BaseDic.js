(function (ng, deepDiff) { 'use strict'; /*jshint validthis: true */

/**
 * BaseDic
 *
 * @module Commons
 *
 * @type Dic
 *
 * @description
 * Holds a collection of entities that comes from the server, and helps manage it,
 * providing retrieval, creation, modification, deletion and WIP functionalities,
 * amongst others.
 *
 * This factory returns the BaseDic ABSTRACT CLASS, NOT an instance.
 * It is meant to be injected wherever we need to create a
 * specific-purpose Dic, by extending it.
 *
 * This file uses the new ES6 syntax (https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes)
 * so it is easier to extend. Since browsers don't fully interpret ES6 yet, during the
 * building phase of this project, all the files are processed and converted to the old ES5,
 * so the browser will see the ES5 version.
 *
 * @usage
 *
 * class ExampleDic extends BaseDic { ... }
 */

commonsModule.factory("Commons.BaseDic", BaseDicFactory);

BaseDicFactory.$inject = ["Restangular", "$timeout"];
function BaseDicFactory(Restangular, $timeout)
{
    class BaseDic
    {
        constructor(resource_name, pk) {
            /**
             * Holds all the Dic instances.
             * @type {Array}
             */
            this.registry = null;

            /**
             * Holds the primary key fieldname.
             * @type {String}
             */
            this.pk = ng.isDefined(pk) ? pk : "id";

            /**
             * Holds the resource name needed for constructing the REST URL.
             * @type {String}
             */
            this.resourceName = resource_name;


            /**
             * Holds the mutex locks. Useful when we are trying to get an instance from a pending retrieval list.
             * @type {Boolean}
             */
            this.__locked = false;
        }

        /**
         * Populates the dictionary with the data in the server.
         */
        loadAll(successCallback, errorCallback) {
            var all,
                _this = this;

            if (this.__isLocked()) {
                $timeout(function(){
                    _this.loadAll(successCallback, errorCallback);
                }, 0);
                return;
            }

            if (this.__isAlreadyCached()) {
                all = this.registry;
                successCallback(all);
            }
            else {
                this.__lock();
                Restangular
                .all(this.resourceName)
                .getList()
                .then(
                    function onSuccess(response) {
                        all = response.data;
                        _this.__registerAll(Restangular.stripRestangular(all));
                        if (successCallback) { successCallback(all); }
                    },
                    function onError(response) {
                        _this.__registerAll([]);
                        if (errorCallback) { errorCallback(response); }
                    }
                ).finally(function() {
                    _this.__unlock();
                });
            }
        }

        /**
         * Returns all the contents of the dictionary.
         *
         * @return {Array}
         */
        getAll() {
            return this.registry;
        }

        /**
         * Returns one instance of the dictionary.
         *
         * @param {String} key
         * Primary key of the instance.
         *
         * @return {Object}
         */
        getInstance(key) {
            var that = this;
            function filterFn(instance) {
                return instance[that.pk] == key;
            }

            var instance = this.registry.filter(filterFn);
            // but TERRIBLE for big amount of registers...

            if (instance.length < 1) {
                return null;
            }

            return instance[0];
        }

        /**
         * Returns a temporary copy (WIP) of the original instance so we can modify the
         * copy and keep reference to the original version at the same time.
         *
         * @param {Object} instance
         * The whole actual instance we want to create the WIP for.
         *
         * @param {bool} createIfNot
         * If `true`, creates the WIP if it doesn't exist. If `false`, it doesn't,
         * and the function will return `null`.
         *
         * @param {bool} clean
         * Clean the meta-info of the WIP before returning it.
         *
         * @return {Object}
         * The WIP. Or `null`.
         */
        getWip(instance, createIfNot, clean) {
            createIfNot = ng.isDefined(createIfNot) ? createIfNot : true;
            clean = ng.isDefined(clean) ? clean : false;

            if (!this.hasWip(instance)) {
                if (!createIfNot) {
                    return null;
                }

                this.__createWip(instance);
            }

            if (clean) {
                return this.__cleanupMeta(instance.$$$wip);
            }

            return instance.$$$wip;
        }

        /**
         * Processes changes between the original instance and its WIP. Notes them internally.
         *
         * @param {Object} instance
         * The whole actual instance we want to check changes for.
         */
        checkForChanges(instance) {
            var wip = this.getWip(instance, false);
            if (!wip) { return; }

            wip.$$$diff = {}; // reset

            var diff = deepDiff(this.__cleanupMeta(instance), this.__cleanupMeta(wip));

            if (!ng.isUndefined(diff)) {
                var dif, path;
                for (var i = 0; i < diff.length; i++) {
                    path = diff[i].path.join(".");
                    /*dif = {
                        kind: diff[i].kind,
                        path: diff[i].path,
                        before : diff[i].lhs,
                        after  : diff[i].rhs,
                    };*/
                    if (ng.isUndefined(wip.$$$diff[path])) {
                        wip.$$$diff[path] = [];
                    }
                    wip.$$$diff[path].push(diff[i]);
                }
            }
        }

        /**
         * Gets the changes between the original instance and its WIP.
         *
         * @param {Object} instance
         * The whole actual instance we want to check changes for.
         *
         * @param {bool} forceCheck
         * If `true`, re-runs `checkForChanges` before returning the changes. Defaults to `false`.
         *
         * @return {Object}
         * Map object with the diff.
         */
        getDiff(instance, forceCheck) {
            forceCheck = !ng.isUndefined(forceCheck) ? forceCheck : false;

            if (forceCheck) {
                this.checkForChanges(instance);
            }

            var wip = this.getWip(instance, false);
            if (!wip) { return {}; }

            return wip.$$$diff;
        }

        /**
         * Checks if an instance has changes.
         *
         * @param {Object} instance
         * The whole actual instance we want to know if it has changes.
         *
         * @param {bool} forceCheck
         * If `true`, re-runs `checkForChanges` before returning the changes. Defaults to `true`.
         *
         * @return {Boolean}
         */
        hasChanges(instance, forceCheck) {
            forceCheck = !ng.isUndefined(forceCheck) ? forceCheck : true;

            return (Object.keys(this.getDiff(instance, forceCheck)).length > 0);
        }

        /**
         * Checks if an instance has WIP.
         *
         * @param {Object} instance
         * The whole actual instance we want to know if it has WIP.

         * @return {Boolean}
         */
        hasWip(instance) {
            return ng.isDefined(instance.$$$wip);
        }

        /**
         * Deletes the instance WIP.
         *
         * @param {Object} instance
         * The whole actual instance we want to delete the WIP of.
         */
        revertChanges(instance) {
            delete instance.$$$wip;
        }

        /**
         * Sends a PUT request to the server with the current instance WIP.
         *
         * @param {Object} instance
         * The whole actual instance we want to save the changes of.
         *
         * @param {Function} successFn
         *
         * @param {Function} errorFn
         */
        saveChanges(instance, successFn, errorFn) {
            var wip = this.getWip(instance, false, true);
            if (!wip) {
                errorFn("error");
            }

            var _this = this;

            Restangular.one(this.resourceName, instance[this.pk]).customPUT(wip).then(
                function onSuccess(response) {
                    _this.revertChanges(instance);
                    ng.extend(instance, Restangular.stripRestangular(response.data));
                    successFn(response.data);
                },
                function onError(response) {
                    errorFn(response.data);
                }
            );
        }

        /**
         * Sends a POST request to the server with the new entity WIP.
         *
         * @param {Function} successFn
         *
         * @param {Function} errorFn
         */
        saveNew(successFn, errorFn) {
            var wip = this.getWip(this.new_entity);
            if (!wip) {
                errorFn("error");
            }

            var _this = this;

            Restangular.one(this.resourceName).customPOST(wip).then(
                function onSuccess(response) {
                    _this.revertChanges(_this.new_entity);
                    _this.registry.push(Restangular.stripRestangular(response.data));
                    successFn(response.data);
                },
                function onError(response) {
                    errorFn(response.data);
                }
            );
        }

        /**
         * Sends a copy request to the server for the instance.
         *
         * @param {Object} instance
         * The whole actual instance we want to copy.
         *
         * @param {Function} successFn
         *
         * @param {Function} errorFn
         */
        copy(instance, successFn, errorFn) {
            var _this = this;

            Restangular.one(this.resourceName, instance[this.pk]).customPOST(null, "copy").then(
                function onSuccess(response) {
                    _this.registry.push(Restangular.stripRestangular(response.data));
                    successFn(response.data);
                },
                function onError(response) {
                    errorFn(response.data);
                }
            );
        }

        /**
         * Sends a lock request to the server for the instance.
         *
         * @param {Object} instance
         * The whole actual instance we want to lock.
         *
         * @param {Function} successFn
         *
         * @param {Function} errorFn
         */
        lock(instance, successFn, errorFn) {
            var _this = this;

            Restangular.one(this.resourceName, instance[this.pk]).customPOST(null, "lock").then(
                function onSuccess(response) {
                    _this.revertChanges(instance);
                    ng.extend(instance, Restangular.stripRestangular(response.data));
                    successFn(response.data);
                },
                function onError(response) {
                    errorFn(response.data);
                }
            );
        }

        /**
         * Sends a DELETE request to the server for the instance.
         *
         * @param {Object} instance
         * The whole actual instance we want to delete.
         *
         * @param {Function} successFn
         *
         * @param {Function} errorFn
         */
        remove(instance, successFn, errorFn) {
            var _this = this;

            Restangular.one(this.resourceName, instance[this.pk]).customDELETE().then(
                function onSuccess(response) {
                    var pos = _this.registry.indexOf(instance);
                    if (pos > -1) {
                        _this.registry.splice(pos, 1);
                    }
                    successFn(response.data);
                },
                function onError(response) {
                    errorFn(response.data);
                }
            );
        }

        /**
         * Gets a field value from the WIP, and fallbacks to the original instance
         * if there is not WIP.
         *
         * @param {Object} instance
         * The whole actual instance we want to get the field value from.
         *
         * @param {String} field
         * The field name.
         *
         * @return {?}
         * The value of the field.
         */
        getWipValueOrFallback(instance, field) {
            var source = this.getWip(instance, false);
            if (!source) {
                source = instance;
            }

            if (ng.isUndefined(source[field])) {
                return "";
            }

            return source[field];
        }

        __isAlreadyCached() {
            return this.registry !== null;
        }

        __registerAll(list) {
            this.registry = list;
        }

        __lock() {
            this.__locked = true;
        }

        __unlock() {
            this.__locked = false;
        }

        __isLocked() {
            return this.__locked;
        }

        __createWip(instance) {
            instance.$$$wip = this.__getNewWip(instance);
        }

        __getNewWip(instance) {
            var wip = ng.copy(instance);
            wip = ng.extend(wip, {
                $$$diff: {},
                $$$changesFlags: !ng.isUndefined(this.changesFlags_defaults) ? ng.copy(this.changesFlags_defaults) : {},
            });
            return wip;
        }

        __cleanupMeta(x) {
            var y = ng.copy(x);
            var key;
            for (key in y) {
                if (key.lastIndexOf("$$$", 0) === 0) {
                    delete y[key];
                }
            }

            return y;
        }
    }

    return BaseDic;
}

})(angular, DeepDiff);
