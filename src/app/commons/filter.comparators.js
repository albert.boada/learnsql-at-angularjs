(function () { 'use strict';

angular.extend(APP.FilterComparators, {
    "typeUnsafeEqualsComparator"     : typeUnsafeEquals,
    "typeUnsafeEqualsLazyComparator" : typeUnsafeEqualsLazy,
    "inArrayComparator"              : inArray,
    "inArrayLazyComparator"          : inArrayLazy,
    "anyInArrayComparator"           : anyInArray,
    "anyInArrayLazyComparator"       : anyInArrayLazy
});

function typeUnsafeEquals(actual, expected) {
    return actual == expected;
}

function typeUnsafeEqualsLazy(filtered, expected) {
    if (!expected && expected !== 0) { return true; }

    return typeUnsafeEquals(filtered, expected);
}

function inArray(needle, haystack) {
    return angular.element.inArray(needle, haystack) != -1;
}

function inArrayLazy(needle, haystack) {
    if (haystack.length === 0) { return true; } // lazy

    return inArray(needle, haystack);
}

function anyInArray(needles, haystack) {
    var found = false;
    angular.forEach(needles, function (needle) {
        if (angular.element.inArray(needle, haystack) != -1) {
            found = true;
        }
    });

    return found;
}

function anyInArrayLazy(needles, haystack) {
    if (haystack.length === 0) { return true; }

    return anyInArray(needles, haystack);
}

})();