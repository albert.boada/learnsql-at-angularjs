(function (ng) { 'use strict';

/**
 * MyTabs Component
 *
 * @description
 * Helps render the same tabs interface in different parts of the app,
 * with configurable choices for each use, and stores the selected tab in the
 * `UiStateManager` global object for global access.
 *
 * @attr {Array} my-tabs-tabs
 * Specifies all the tabs. Should have the following schema: `[{slug: "", name: ""}, ...]`.
 *
 * @attr {String} my-tabs-model
 * Specifies the key for `UiStateManager` in which the selected tag slug will be stored.
 *
 * @attr {Function} my-tabs-disabled
 * Specifies the function that should be called for each tab in `my-tabs-tabs`
 * in order to check if the tab should be disabled or not. This function needs to
 * accept one parameter, which will be the slug of the tab being checked.
 *
 * @usage
 *
 * scope = {
 *     tabs: [{slug: "tab1", name: "Tab 1"}, {slug: "tab2", name: "Tab 2"}],
 *     isDisabled: function (slug) { return false; },
 * }
 *
 * <MY-TABS my-tabs-tabs="tabs"
 *          my-tabs-model="selectedTab"
 *          my-tabs-disabled="isDisabled(_param)"
 * ></MY-TABS>
 *
 * @tip
 * To programatically alter the selected tag, just modify the adequate
 * `UiStateManager` key.
 *
 * @tip
 * For performance reasons this component will not react to dinamically added
 * tabs after creation. To enable this, remove the `::` in the
 * `ng-repeat="tab in ::myTabs.tabs"` in the template.
 */

commonsModule.component("myTabs", {
    bindings: {
        tabs       : "=myTabsTabs",
        modelKey   : "@myTabsModel",
        isDisabled : "&myTabsDisabled",
    },
    controller: MyTabsCtrl, controllerAs: "myTabs",
    template: `

        <ul class="nav nav-tabs">
            <li ng-repeat="tab in ::myTabs.tabs"
                ng-class="{active: myTabs.isCurrent(tab.slug), disabled: myTabs.isDisabled({_param: tab.slug})}"
            >
                <a ng-click="myTabs.changeTab(tab.slug)" href="" translate>{{tab.name}}</a>
            </li>
        </ul>

    `
});

/**
 * MyTabs Controller
 */
MyTabsCtrl.$inject = ["$attrs", "Commons.UiStateManager"];
function MyTabsCtrl($attrs, UiStateManager) {
    var VM = this;
    ng.extend(VM, {
        /**
         * Model
         *
         * The following properties are automatically available in the VM because
         * of the Component's `bindings`:
         * tabs,
         * modelKey,
         * isDisabled,
         */

        /**
         * Actions
         */
        isCurrent: isCurrent,
        changeTab: changeTab
    });

    function isCurrent(slug) {
        return slug == UiStateManager.get(VM.modelKey);
    }

    function changeTab(slug) {
        if (!VM.isDisabled({_param: slug})) {
            UiStateManager.set(VM.modelKey, slug);
        }
    }
}

})(angular);