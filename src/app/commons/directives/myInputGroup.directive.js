(function (ng) { 'use strict';

/**
 * MyInputGroup Directive
 *
 * @description
 * Makes laying out Twitter Bootsrap style inputs easier.
 * http://getbootstrap.com/
 * This directive is specifically in charge of the wrapper of the input.
 * Meant to be used from the `myInput` directive, or from anywhere else when
 * the contents of the wrapper needs to be special.
 *
 * @attr {String} my-input-group-label
 * @attr {String} my-input-group-label-for
 *
 * @usage
 *
 * <MY-INPUT-GROUP my-input-group-label="MESSAGE"
 *                 my-input-group-label-for="foo"
 * >
 *     <textarea id="foo"></textarea>
 *     <!-- This is a random comment that will make it to the result -->
 * </MY-INPUT-GROUP>
 *
 * // result:
 *
 * <div class="form-group">
 *     <label class="control-label" for="foo" translate>MESSAGE</label>
 *     <textarea id="foo"></textarea>
 *     <!-- This is a random comment that will make it to the result -->
 * </div>
 */

commonsModule.directive("myInputGroup", myInputGroupDirectiveFactory);

function myInputGroupDirectiveFactory() {
    return {
        scope: false,
        restrict: 'E',
        transclude: true,
        template: function (element, attrs) {
            var groupClass = ["form-group"];
            if (attrs.hasOwnProperty('small')) {
                groupClass.push("form-group-sm");
            }
            var forid = attrs.hasOwnProperty('myInputGroupLabelFor') ? "for=\""+attrs.myInputGroupLabelFor+"\"" : "";
            var label = attrs.hasOwnProperty('myInputGroupLabel') ? '<label class="control-label" '+forid+' translate>'+attrs.myInputGroupLabel+'</label>' : "";

            var htmlText = "";
            htmlText += '<div class="'+groupClass.join(" ")+'">';
            htmlText +=     label;
            htmlText +=     '<ng-transclude></ng-transclude>';
            htmlText += '</div>';

            return htmlText;
        }
    };
}

})(angular);