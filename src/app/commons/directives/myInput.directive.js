(function (ng) { 'use strict';

/**
 * MyInput Directive
 *
 * @description
 * Makes laying out Twitter Bootsrap style inputs easier.
 * http://getbootstrap.com/
 *
 * @attr {String} my-input-type
 * @attr {String} my-input-id
 * @attr {String} my-input-model
 * @attr {String} my-input-options
 * @attr {String} my-input-multiple
 * @attr {String} my-input-label
 * @attr {String} my-input-placeholder
 *
 * @usage
 *
 * // Example 1: Text
 * <MY-INPUT my-input-type="text"
 *           my-input-id="input-title"
 *           my-input-model="VM.title"
 *           my-input-label="TITLE"
 * ></MY-INPUT>
 *
 * // Example 1 result:
 * <div class="form-group">
 *     <label class="control-label" for="input-title" translate>TITLE</label>
 *     <input type="text" id="input-title" ng-model="VM.title" class="form-control" />
 * </div>
 *
 * // Example 2: Select
 * <MY-INPUT my-input-type="select"
 *           my-input-id="input-countries"
 *           my-input-model="VM.countriesIds"
 *           my-input-options="c.id as c.name for c in VM.countries"
 *           my-input-multiple
 *           my-input-label="COUNTRIES"
 * ></MY-INPUT>
 *
 * // Example 2 result:
 *
 * <div class="form-group">
 *     <label class="control-label" for="input-countries" translate>COUNTRIES</label>
 *     <select id="input-countries" ng-model="VM.countriesIds" ng-options="c.id as c.name for c in VM.countries" multiple class="form-control" />
 * </div>
 */

commonsModule.directive("myInput", myInputDirectiveFactory);

function myInputDirectiveFactory() {
    return {
        scope: false,
        restrict: 'E',
        template: function (element, attrs) {
            var type = attrs.myInputType || 'text';
            var required = attrs.hasOwnProperty('required') ? 'required="required"' : "";
            var ngModel = attrs.hasOwnProperty('myInputModel') ? 'ng-model="'+attrs.myInputModel+'"' : "";
            var inputClass = ["form-control"];
            var placeholder = attrs.hasOwnProperty('myInputPlaceholder') ? 'placeholder="{{\''+attrs.myInputPlaceholder+'\' | translate}}"' : "";
            var label = attrs.hasOwnProperty('myInputLabel') ? attrs.myInputLabel : "";
            var id = attrs.hasOwnProperty('myInputId') || "input-"+label.replace(" ", "")+Math.floor(Math.random()*999999999);
            var small = attrs.hasOwnProperty('small') ? 'small' : "";
            var ngOptions = attrs.hasOwnProperty('myInputOptions') ? 'ng-options="'+attrs.myInputOptions+'"' : "";
            var multiple = attrs.hasOwnProperty('myInputMultiple') ? 'multiple="multiple"' : "";

            var htmlText = "";

            htmlText += '<MY-INPUT-GROUP my-input-group-label="'+label+'" my-input-group-label-for="'+id+'" '+small+'>';

            if (type == "select") {
                htmlText += '<select '+multiple+' class="'+inputClass.join(" ")+'" '+ngModel+' '+ngOptions+' id="'+id+'" '+required+' '+placeholder+'></select>';
            } else {
                htmlText += '<input type="'+type+'" class="'+inputClass.join(" ")+'" '+ngModel+' id="'+id+'" '+required+' '+placeholder+' />';
            }

            htmlText += '</MY-INPUT-GROUP>';

            return htmlText;
        }
    };
}

})(angular);