(function (ng) { 'use strict';

/**
 * UiStateManager
 *
 * @module Commons
 *
 * @type Service
 *
 * @description
 * This service is used to store any global state (simple values or objects)
 * that needs to be obtained in the future, from anywhere in the app.
 *
 * @usage
 *
 * // Simple value
 * UiStateManager.set("foo", 3);
 * var foo = UiStateManager.get("foo"); // foo == 3
 *
 * // Object value
 * UiStateManager.set("bar", {id: 3, title: "Hello world"});
 * var title = UiStateManager.get("bar.title"); // title == "Hello world"
 * var bar   = UiStateManager.get("bar");       // bar == {id: 3, title: "Hello world"}
 */

commonsModule.service("Commons.UiStateManager", UiStateManager);

function UiStateManager()
{
    /**
     * Store.
     *
     * @private
     *
     * @type {Object}
     */
    var _store = {};

    /**
     * Gets a value from the store.
     *
     * @public
     *
     * @param {String} path
     * Path where the value we want to query is located in the store.
     *
     * @param {bool} shouldcreatepath
     * If `true`, creates an empty object (`{}`) in the specified path, if it did not exist before.
     * Default is `false`.
     *
     * @return {Object}
     * The value we are querying for.
     */
    this.get = function get(path, shouldcreatepath) {
        shouldcreatepath = ng.isUndefined(shouldcreatepath) ? false : shouldcreatepath;
        var tmp = _store;
        path = path.split('.');
        for (var i = 0; i < path.length; i++) {
            if (ng.isDefined(tmp[path[i]])) {
                tmp = tmp[path[i]];
            } else {
                if (shouldcreatepath) {
                    tmp[path[i]] = {};
                    tmp = tmp[path[i]];
                } else {
                    return undefined;
                }
            }
        }

        return tmp;
    };

    /**
     * Checks if a path exists in the store.
     *
     * @public
     *
     * @param {String} path
     * Path where the value we want to query is located in the store.
     *
     * @return {bool}
     * `true` if the path exists, `false` if not.
     */
    this.exists = function exists(path) {
        return ng.isDefined(this.get(path, false));
    };

    /**
     * Sets a value in the store.
     *
     * @public
     *
     * @param {String} path
     * Path where the value we want to query is located in the store.
     *
     * @param {Object} value
     * Value to store in the specified path of the store.
     *
     * @param {bool} shouldoverwrite
     * If `true`, overwrites the existing value in the specified path. Default is `true`.
     */
    this.set = function set(path, value, shouldoverwrite) {
        shouldoverwrite = ng.isDefined(shouldoverwrite) ? shouldoverwrite : true;
        var tmp = _store;
        path = path.split('.');
        for (var i = 0; i < path.length - 1; i++) {
            tmp = tmp[path[i]] ? tmp[path[i]] : tmp[path[i]] = {};
        }

        if (shouldoverwrite || ng.isUndefined(tmp[path[path.length - 1]])) {
            tmp[path[path.length - 1]] = value;
        }
    };

    /**
     * Sets a value in the store, if there is not one yet in the specified path.
     *
     * @public
     *
     * @param {String} path
     * Path where the value we want to query is located in the store.
     *
     * @param {Object} value
     * Value to store in the specified path of the store.
     */
    this.setDefault = function setIfNotSet(path, value) {
        this.set(path, value, false);
    };
}

})(angular);