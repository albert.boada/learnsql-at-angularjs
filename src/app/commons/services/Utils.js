(function (ng) { 'use strict';

/**
 * Utils
 *
 * @module Commons
 *
 * @type Service
 *
 * @description
 * This service provides a set of utilities that are needed throughout the application.
 */

commonsModule.service("Commons.Utils", Utils);

Utils.$inject = [
"appSettings",
"$q",
"$state",
"$timeout",
"$translate"];
function Utils(appSettings, $q, $state, $timeout, $translate)
{
    return {
        isCreateRouteKeyword  : isCreateRouteKeyword,
        resolveEntitiesList   : resolveEntitiesList,
        goWithoutReload       : goWithoutReload,
        execAfterCurrentCycle : execAfterCurrentCycle,
        alertValidationError  : alertValidationError,
    };

    /**
     * Tells if the specified keyword is the same keyword used for identifying
     * new entities in the URL.
     *
     * @public
     *
     * @param {String} keyword
     */
    function isCreateRouteKeyword(keyword) {
        return appSettings.routeCreateKeyword == keyword;
    }

    /**
     * Defers the asynchronous population of a dictionary (this population needs to
     * query the server) until it has finished. Useful in route-resolves in `app.states.js`.
     * See https://github.com/angular-ui/ui-router/wiki#resolve
     *
     * @public
     *
     * @param  {Dic} Dic
     * Dictionary we want to populate with data from the server.
     *
     * @return {Promise}
     * Promise that will be resolved when the population of the dictionary is complete.
     */
    function resolveEntitiesList(Dic) {
        var deferred = $q.defer();
        Dic.loadAll(__successCallback, __errorCallback);

        return deferred.promise;

        function __successCallback(result) {
            if (result) {
                deferred.resolve(result);
            } else {
                deferred.resolve();
            }
        }

        function __errorCallback(error) {
            deferred.resolve();
            console.log("ERROR: Error fetching data from the server.");
            console.log(error);
        }
    }

    /**
     * ui-router can not transition to the same current state with different parameters (e.g.
     * from `/questions/10` to `/questions/18`) without re-creating the controller(s) and view(s) involved.
     * See: https://github.com/angular-ui/ui-router/issues/1758
     * In some occasions, we are interested in preserving the controller and view, and just change
     * the data they are rendering, when switching from one entity to another. This helper method
     * provides this functionality by faking some of the steps in the state change process.
     *
     * @param {String} transitionTo
     * Identifier of the state we want to transition to. Normally it will be the same state we are in.
     *
     * @param {String} localName
     * Identifier of the `ui-view` that holds the `ui-view` in which the pivot state is embeded.
     * In the /questions/10 context, that url belongs to the "question" state. This state is embeded in
     * the "questionOutlet" `ui-view`, which happens to be in the "mainOutlet" from the "root" state.
     * So localName = "mainOutlet@root".
     *
     * @param {int} howDeepIsTheState
     * How deep the state is in the states hierarchy, starting from 0.
     *
     * @param {Function} callback
     * Optional. Function that will be called after the transition.
     */
    function goWithoutReload(transitionTo, localName, howDeepIsTheState, callback) {
        var previousLocals = $state.$current.path[howDeepIsTheState].locals[localName] || {};

        $state.go(transitionTo.name, transitionTo.params, {notify: false});

        $timeout(function () {
            var newLocals = $state.$current.path[howDeepIsTheState].locals[localName];
            previousLocals.$stateParams = newLocals.$stateParams;
            $state.$current.path[howDeepIsTheState].locals[localName] = previousLocals;
            // ^ https://github.com/angular-ui/ui-router/issues/1758#issuecomment-175339917

            if (ng.isDefined(callback)) {
                callback();
            }
        });
    }

    /**
     * Delays execution of the specified callback until the angular UI rendering cycle
     * has finished. Useful when tweaking the model, wanting the changes to be reflected
     * in the view right away, and doing something else after that.
     *
     * @param {Function} callback
     */
    function execAfterCurrentCycle(callback) {
        $timeout(callback, 0);
    }

    /**
     * Composes, and finally alerts, a form validation message.
     *
     * @param {String} error
     * Error code.
     *
     * @param {Object} error_vars
     * Variables to add to the translation of the error code.
     *
     * @param {String} path
     * Path of the errored field.
     *
     * @param {String} field
     * Errored field identifier.
     */
    function alertValidationError(error, error_vars, path, field) {
        var msg = "";
        msg += $translate.instant("SAVE_VALIDATION_ERROR");
        msg += "\n\n";
        if (field) {
            msg += (path ? path+"." : "")+$translate.instant(field).toUpperCase();
            msg += "\n\n";
        }
        msg += $translate.instant("VALIDATION_"+error, error_vars);

        alert(msg);
    }

    /**
     *
     */
    function alertServerError(error) {
        if (ng.isDefined(error.exception)) {
            alert("Server returned an Exception:\n"+error.exception);
        } else {
            alert("Server gave an unexpected answer:\n"+JSON.stringify(error));
        }
    }
}

})(angular);