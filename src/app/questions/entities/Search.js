(function () {

/**
 * Search
 *
 * @module Questions
 *
 * @type Entity
 *
 * @description
 * Stores the filter criteria for a question.
 */

questionsModule.factory("Questions.Search", SearchFactory);

function SearchFactory() {
    return Search;
}

function Search()
{
    var _this = this;

    var _defaults = {
        id             : "",
        title          : "",
        author         : "",
        difficulty_min : 0,
        difficulty_max : 1,
        categories     : [],
        thematics      : [],
        schemas        : [],
        resolved       : "",
        dirty          : "",
    };

    reset();

    this.reset = reset;
    this.isEmpty = isEmpty;

    return this;

    function reset() {
        _this.id             = _defaults.id;
        _this.title          = _defaults.title;
        _this.author         = _defaults.author;
        _this.difficulty_min = _defaults.difficulty_min;
        _this.difficulty_max = _defaults.difficulty_max;
        _this.categories     = _defaults.categories;
        _this.thematics      = _defaults.thematics;
        _this.schemas        = _defaults.schemas;
        _this.resolved       = _defaults.resolved;
        _this.dirty          = _defaults.dirty;
    }

    function isEmpty() {
        // jshint ignore:start
        return (
            _this.id                == _defaults.id
            && _this.title          == _defaults.title
            && _this.author         == _defaults.author
            && _this.difficulty_min == _defaults.difficulty_min
            && _this.difficulty_max == _defaults.difficulty_max
            && _this.categories     == _defaults.categories
            && _this.thematics      == _defaults.thematics
            && _this.schemas        == _defaults.schemas
            && _this.resolved       == _defaults.resolved
            && _this.dirty          == _defaults.dirty
        );
        // jshint ignore:end
    }
}

})();