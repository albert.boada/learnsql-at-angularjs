(function (States) { 'use strict';

States.questions =

    {   name     : "questions",
        parent   : "root",
        views    : {
            "mainOutlet": {
                template: "<QUESTIONS></QUESTIONS>"
        }},
        children : [

            {   name     : "question",
                abstract : "questionForm", // true
                params   : {"id": {value: null}}, // allows us to 1) working without URLs, and 2) not having to define a URL for this abstract state if we are working with URLs :) We just borrow the {id} URL param from the leaf child state where the actual final URL is defined :)
                views    : {
                    "questionOutlet": {
                        template: "<QUESTION></QUESTION>"
                }},
                children : [

                    {   name  : "questionForm",
                        views : {
                            "questionContentOutlet": {
                                template: "<QUESTION-FORM></QUESTION-FORM>"
                    }}},


                    {   name  : "questionExecute",
                        views : {
                            "questionContentOutlet": {
                                template: "<QUESTION-EXECUTE></QUESTION-EXECUTE>"
                    }}}

                ]
            }
        ]
    }

;

})(APP.States);
