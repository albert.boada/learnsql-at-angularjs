(function (ng) { 'use strict';

questionsModule.factory("Questions.QuestionsDic", QuestionsDicFactory);

QuestionsDicFactory.$inject = ["Commons.BaseDic", "Questions.TestsetsService", "Questions.ChecksService"];
function QuestionsDicFactory(BaseDic, TestsetsService, ChecksService)
{
    class QuestionsDic extends BaseDic
    {
        constructor() {
            super("questions");

            this.new_entity = {
                id: 0,
                titulo: "",
                autor: "",
                enunciado: "",
                dificultad: 0.5,
                disponible: false,
                bloqueada: false,
                binaria: null,
                gabia: null,
                inits: "",
                solucion: "",
                limpieza: "",
                postinits: "",
                postInitsJP: 0,
                url: "",
                usuario: "",
                clave: "",
                ssl: false,
                ficheroAdjunto: null,
                ficheroSolucion: null,
                tiposolucionId: 1,
                categoriaId: null,
                tematicasIds: [],
                esquemaId: null,
                juegosPruebas: [],
            };

            this.changesFlags_defaults = {
                question: false,
                testsetsAdded: [],
                testsetsModified: [],
                testsetsRemoved: [],
                checksAdded: [],
                checksModified: [],
                checksRemoved: [],
            };
        }

        findTestsetByIndex(index, question) {
            return TestsetsService.findByIndex(index, question.juegosPruebas);
        }

        findTestsetById(id, question) {
            return TestsetsService.findById(id, question.juegosPruebas);
        }

        hasTestsets(question) {
            return TestsetsService.has(question.juegosPruebas);
        }

        /**
         * ChangesFlags set of functions
         *
         * Used to keep track and summarize what has the user changed of a Question,
         * its Testsets, and their Checks.
         */

        registerQuestionInfoModified(wip) {
            wip.$$$changesFlags.question = true;
        }

        deregisterQuestionInfoModified(wip) {
            wip.$$$changesFlags.question = false;
        }

        registerTestsetsChange(mode, testset_id, wip) {
            var _flags = wip.$$$changesFlags;

            if (_flags["testsets"+mode].indexOf(testset_id) != -1) { // check if it's already registered
                return;
            }

            if (mode == "Added" || mode == "Modified") {
                _flags["testsets"+mode].push(testset_id);
            }
            else if (mode == "Removed") {
                var added_index    = _flags.testsetsAdded.indexOf(testset_id);
                var modified_index = _flags.testsetsModified.indexOf(testset_id);

                if (added_index != -1) {
                    _flags.testsetsAdded.splice(added_index, 1);
                }
                else {
                    if (modified_index != -1) {
                        _flags.testsetsModified.splice(modified_index, 1);
                    }
                    _flags.testsetsRemoved.push(testset_id); // :D
                }

                // Get rid also of any Checks flag related to the removed Testset
                var i, j, flag, cks_ids, ck_id;
                var ck_flags = ["checksAdded", "checksModified", "checksRemoved"];
                for (i = 0; i < ck_flags.length; i++) {
                    flag = ck_flags[i];
                    cks_ids = _flags[flag];
                    for (j = cks_ids.length - 1; j >= 0; j--) { // backwards cuz splicing the very same array in the loop
                        ck_id = cks_ids[j];
                        if (ck_id.substr(0, testset_id.length + 1) == testset_id+"-") {
                            cks_ids.splice(j, 1);
                        }
                    }
                }
            }
        }

        deregisterTestsetsChange(mode, testset_id, wip) {
            var found = wip.$$$changesFlags["testsets"+mode].indexOf(testset_id);
            if (found != -1) {
                wip.$$$changesFlags["testsets"+mode].splice(found, 1);
            }
        }

        registerChecksChange(mode, check_id, testset_id, wip) {
            if (testset_id.match(/^\$\$\$new/)) {
                return;
            }

            var key = testset_id+"-"+check_id;
            var _flags = wip.$$$changesFlags;

            if (_flags["checks"+mode].indexOf(key) != -1) { // check if it's already registered
                return;
            }

            if (mode == "Added" || mode == "Modified") {
                _flags["checks"+mode].push(key);
            }
            else if (mode == "Removed") {
                var added_index = _flags.checksAdded.indexOf(key);
                var modified_index = _flags.checksModified.indexOf(key);
                if (added_index != -1) {
                    _flags.checksAdded.splice(added_index, 1);
                }
                else {
                    if (modified_index != -1) {
                        _flags.checksModified.splice(modified_index, 1);
                    }

                    _flags.checksRemoved.push(key);
                }
            }
        }

        deregisterChecksChange(mode, check_id, testset_id, wip) {
            var found = wip.$$$changesFlags["checks"+mode].indexOf(testset_id+"-"+check_id);
            if (found != -1) {
                wip.$$$changesFlags["checks"+mode].splice(found, 1);
            }
        }
    }

    return new QuestionsDic();
}

})(angular);