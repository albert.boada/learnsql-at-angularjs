(function () { 'use strict';

questionsModule.factory("Questions.SolutiontypesDic", SolutiontypesDicFactory);

SolutiontypesDicFactory.$inject = ["Commons.BaseDic"];
function SolutiontypesDicFactory(BaseDic)
{
    class SolutiontypesDic extends BaseDic
    {
        constructor() {
            super("solutiontypes");
        }
    }

    return new SolutiontypesDic();
}

})();