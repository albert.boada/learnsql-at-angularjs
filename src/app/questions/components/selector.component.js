(function (ng) { 'use strict';

questionsModule.component("questionsSelector", {
    require: {
        questionsComponent: "^^questions",
    },
    controller: QuestionsSelectorCtrl, controllerAs: "VM",
    templateUrl: "questions/components/selector.tpl.html"
});

QuestionsSelectorCtrl.$inject = [
"Questions.QuestionsDic",
"Categories.CategoriesDic",
"Thematics.ThematicsDic",
"Schemas.SchemasDic",
"Questions.Search",
"Commons.UiStateManager",
"$filter",
"$scope"];
function QuestionsSelectorCtrl(QuestionsDic, CategoriesDic, ThematicsDic, SchemasDic, Search, UiStateManager, $filter, $scope)
{
    var _questionsComponent, _questions, _current;

    var VM = this;
    ng.extend(VM, {
        /**
         * Model
         */
        questions: [],
        closed: false,
        shouldShowSearch: false,
        search: null,
        resolved_options: [
            { val: '', name: '-' },
            { val: true, name: 'RESOLVED' },
            { val: false, name: 'UNRESOLVED' },
        ],
        dirty_options: [
            { val: '', name: '-' },
            { val: true, name: 'DIRTYF' },
            { val: false, name: 'NOTDIRTYF' },
        ],
        categories: [],
        thematics: [],
        schemas: [],
        num_questions_total: 0,
        num_questions_showing: 0,

        /**
         * Actions
         */
        toggle: toggle,
        toggleSearch: toggleSearch,
        getFilteredQuestions: getFilteredQuestions,
        addNewQuestion: addNewQuestion,
        pickQuestion: pickQuestion,
        isCurrentQuestion: isCurrentQuestion,
        getQuestionField: getQuestionField,
        isQuestionDirty: isQuestionDirty,
    });

    this.$onInit = componentDidMount;

    function componentDidMount() {
        _questionsComponent = VM.questionsComponent;
        delete VM.questionsComponent;

        _questions = _questionsComponent.questions;
        VM.num_questions_total   =
        VM.num_questions_showing = _questions.length;

        UiStateManager.setDefault("questions.search", new Search());
        VM.search = UiStateManager.get("questions.search");

        refresh();

        VM.categories = CategoriesDic.getAll();
        VM.thematics = ThematicsDic.getAll();
        VM.schemas = SchemasDic.getAll();

        $scope.$on("questionSelected", onQuestionSelected);
        $scope.$watch(function () { return VM.search; }, onSearchModified, true); // true!!
    }

    function refresh() {
        _current = UiStateManager.get("questions.question");

        VM.num_questions_total = _questions.length;

        refresh2();
    }

    function refresh2() {
        VM.questions = getFilteredQuestions();
        VM.num_questions_showing = VM.questions.length;
    }

    function getFilteredQuestions() {
        /*
        var searched_qs = _questions;
        searched_qs = filterFilter(searched_qs, {id: VM.search.id}, APP.FilterComparators.typeUnsafeEqualsLazyComparator);
        searched_qs = filterFilter(searched_qs, {titulo: VM.search.title, autor: VM.search.author, disponible: VM.search.solved});
        // searched_qs = filterFilter(searched_qs, {categoria: VM.search.categories, esquema: VM.search.schemas}, $scope.inArrayLazyComparator);
        // searched_qs = filterFilter(searched_qs, {tematicas: VM.search.thematics}, $scope.anyInArrayLazyComparator);
        searched_qs = filterFilter(searched_qs, isInBetweenLazyFilterer((VM.search.difficulty_min === 0 ? -1 : VM.search.difficulty_min), VM.search.difficulty_max));
        */

        if (VM.search.isEmpty()) {
            return _questions;
        } else {
            return _questions.filter(questionsFilterFn);
        }

        return searched_qs;
    }

    function toggle() {
        VM.closed = !VM.closed;
    }

    function toggleSearch() {
        VM.shouldShowSearch = !VM.shouldShowSearch;
    }

    function addNewQuestion() {
        $scope.$emit("doNewQuestion"); // up!
    }

    function pickQuestion(id) {
        $scope.$emit("doGoToQuestion", id); // up!
    }

    function isCurrentQuestion(id) {
        return _current && _current.id == id;
    }

    function getQuestionField(question, field) {
        return QuestionsDic.getWipValueOrFallback(question, field);
    }

    function isQuestionDirty(question) {
        return QuestionsDic.hasChanges(question, false);
    }

    function onQuestionSelected(e, data) {
        refresh();
    }

    function onSearchModified() {
        refresh2();
    }

    function questionsFilterFn(q) {
        var s = VM.search;
        // jshint ignore:start

        return (
            (!s.id || q.id == s.id)
            && (s.title === "" || fuzzy(/*getQuestionField(q, "titulo")*/q.titulo, s.title))
            && (s.author === "" || fuzzy(/*getQuestionField(q, "autor")*/q.autor, s.author))
            && (s.resolved === "" || q.disponible == s.resolved)
            && (s.dirty === "" || QuestionsDic.hasChanges(q, false) === s.dirty)
            && (/*getQuestionField(q, "dificultad")*/q.dificultad >= (s.difficulty_min === 0 ? -1 : s.difficulty_min) && /*getQuestionField(q, "dificultad")*/q.dificultad <= s.difficulty_max)
            && (!s.categories.length || s.categories.indexOf(q.categoriaId) != -1)
            && (!s.thematics.length || APP.FilterComparators.anyInArrayComparator(s.thematics, q.tematicasIds))
            && (!s.schemas.length || s.schemas.indexOf(q.esquemaId) != -1)
        );
        // jshint ignore:end
    }

    function fuzzy(actual, expected){
        actual = $filter('lowercase')('' + actual);
        expected = $filter('lowercase')('' + expected);
        return actual.indexOf(expected) !== -1;
    }

    /**
     * filter:between(min_val, max_val)
     */
    function isInBetweenLazyFilterer(min, max) {
        return function (question) {
            if ((min || min === 0 || min === "0") && question.dificultad < min) {
                return false;
            }
            if ((max || max === 0 || max === "0") && question.dificultad > max) {
                return false;
            }

            return true;
        };
    }
}


})(angular);