(function (ng) { 'use strict';

questionsModule.component("questionTestsetChecks", {
    controller: QuestionTestsetChecksCtrl, controllerAs: "VM",
    templateUrl: "questions/components/checks.tpl.html"
});

QuestionTestsetChecksCtrl.$inject = [
"Questions.QuestionsDic",
"Questions.ChecksService",
"Commons.UiStateManager",
"Commons.Utils",
"$scope",
"$rootScope"];
function QuestionTestsetChecksCtrl(QuestionsDic, ChecksService, UiStateManager, Utils, $scope, $rootScope)
{
    var VM = this;
    ng.extend(VM, {
        /**
         * Model
         */
        checks : [],
        num_total: 0,

        /**
         * Actions
         */
        pick      : pick,
        isCurrent : isCurrent,
        addNew    : addNew,
        canRemove : canRemove,
        removeCurrent : removeCurrent,
    });

    var _testset, _testset_index, _current, _questionwip;

    componentDidMount();

    /***************************************************************************
     * Implementation
     **************************************************************************/

    function componentDidMount() {
        refresh();

        $scope.$watch(function () { return VM.checks.length; }, onChecksLengthChanged);
        $scope.$on("testsetSelected", onTestsetSelected);
        $scope.$on("doDeleteCurrentCheck", onDoDeleteCurrentCheck);
        $scope.$on("goToCheck", onGoTo);
    }

    function refresh()  {
        var q = UiStateManager.get("questions.question");
        if (q) {
            _questionwip = QuestionsDic.getWip(q);
            _testset = null;
            _testset_index = null;

            if (_questionwip) {
                _testset_index = UiStateManager.get("questions.testsetIndex");
                _testset = QuestionsDic.findTestsetByIndex(_testset_index, _questionwip);
            }

            var index = null;
            VM.checks = [];
            if (_testset) {
                VM.checks = _testset.comprobaciones;
                index = obtainCurrent();
            }

            VM.num_total = VM.checks.length;

            setCurrent(index);
        }
    }

    function obtainCurrent() {
        var index = UiStateManager.get("questions.cksindices."+_questionwip.id+"."+_testset_index);
        /*if (ng.isUndefined(index)) {
            index = 0;
        }*/

        if (ng.isUndefined(VM.checks[index])) {
            return null;
        }

        return index;
    }

    function onTestsetSelected() {
        refresh();
    }

    function onDoDeleteCurrentCheck(e) {
        removeCurrent();
    }

    function onChecksLengthChanged(current, old) {
        if (current != old) {
            VM.num_total = current;
        }
    }

    function onGoTo(e, index, callback) {
        pick(index);
        Utils.execAfterCurrentCycle(callback);
    }

    function pick(index) {
        setCurrent(index);
    }

    function isCurrent(index) {
        return _current == index;
    }

    function addNew() {
        var ck =
        ChecksService.addNew("Nova Compr. (#{{counter}})", VM.checks);
        setCurrent(VM.checks.length - 1);

        QuestionsDic.registerChecksChange("Added", ck.id, _testset.id, _questionwip);

        $scope.$broadcast("checkAdded");
    }

    function canRemove() {
        return _current != null;
    }

    function removeCurrent() {
        deleteCheck(_current);
    }

    function deleteCheck(index) {
        var ck_deleted =
        ChecksService.deleteByIndex(index, VM.checks);

        // Update default name new instances
        for (var i = index; i < VM.checks.length; i++) {
            var ck = VM.checks[i];
            if (ck.nombre.match(/Nova Compr\. \(#[\d]+\)/)) {
                ck.nombre = "Nova Compr. (#"+(i+1)+")";
            }
        }

        QuestionsDic.registerChecksChange("Removed", ck_deleted.id, _testset.id, _questionwip);

        setCurrent(null);
    }

    function setCurrent(index) {
        _current = index;
        if (_testset) {
            UiStateManager.set("questions.cksindices."+_questionwip.id+"."+_testset_index, index);
        }
        broadcast(index);
    }

    function broadcast(index) {
        UiStateManager.set("questions.checkIndex", index); // Make it available globally (for all child components)
        $scope.$broadcast("checkSelected");
    }
}


})(angular);