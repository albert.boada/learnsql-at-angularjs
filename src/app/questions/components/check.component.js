(function (ng) { 'use strict';

questionsModule.component("questionTestsetCheck", {
    controller: QuestionTestsetCheckCtrl, controllerAs: "VM",
    templateUrl: "questions/components/check.tpl.html"
});

/**
 * QuestionTestsetCheckCtrl
 */
QuestionTestsetCheckCtrl.$inject = [
"Questions.QuestionsDic",
"Questions.TestsetsService",
"Commons.UiStateManager",
"$scope"];
function QuestionTestsetCheckCtrl(QuestionsDic, TestsetsService, UiStateManager, $scope)
{
    var VM = this;
    ng.extend(VM, {
        /**
         * Model
         */
        check: null,
        sections: [
            {name: "GENERAL_INFO", slug: "general"},
            {name: "SQL_STATEMENTS", slug: "sql"},
        ],
        booleanOptions: [{key: null, val: "-"}, {key: true, val: "YES"}, {key: false, val: "NO"}],

        /**
         * Actions
         */
        remove: remove,
        shouldShowSection: shouldShowSection,
        showErrormsg: showErrormsg,
        shouldShowErrormsg: shouldShowErrormsg,
    });

    var _questionwip, _testset;

    componentDidMount();

    /***************************************************************************
     * Implementation
     **************************************************************************/

    function componentDidMount() {
        refresh();

        // Set default visible section
        UiStateManager.setDefault("questions.checkSection", VM.sections[0].slug); // @todo move to state on successful navigation?

        UiStateManager.setDefault("questions.showCheckErrormsg", "ca"); // @todo move to state on successful navigation?

        $scope.$on("testsetSelected", onTestsetSelected);
        $scope.$on("checkSelected", onCheckSelected);
        $scope.$on("checkAdded", onCheckAdded);

        $scope.$watch(function () { return VM.check; }, onCheckModified, true); // true!!
    }

    function remove() {
        $scope.$emit("doDeleteCurrentCheck"); // up!
    }

    function shouldShowSection(slug) {
        return UiStateManager.get("questions.checkSection") == slug;
    }

    function shouldShowErrormsg(lang) {
        return UiStateManager.get("questions.showCheckErrormsg") == lang;
    }

    function showErrormsg(lang) {
        UiStateManager.set("questions.showCheckErrormsg", lang);
    }

    function refresh() {
        var q = UiStateManager.get("questions.question");
        if (q) {
            _questionwip = QuestionsDic.getWip(q);
            _testset = null;

            if (_questionwip) {
                var testset_index = UiStateManager.get("questions.testsetIndex");
                _testset = QuestionsDic.findTestsetByIndex(testset_index, _questionwip);
            }

            refresh2();
        }
    }

    function refresh2() {
        VM.check = null;

        if (_testset) {
            var check_index = UiStateManager.get("questions.checkIndex");
            VM.check = TestsetsService.findCheckByIndex(check_index, _testset);
        }
    }

    function onCheckModified(_new, _old) {
        if (_new && _old && _new.id == _old.id) {

            QuestionsDic.deregisterChecksChange("Modified", VM.check.id,  _testset.id, _questionwip); // reset before checking again

            var question_original = UiStateManager.get("questions.question");
            var ts_original = QuestionsDic.findTestsetById(_testset.id, question_original);
            if (ts_original) {
                var ck_original = TestsetsService.findCheckById(VM.check.id, ts_original);
                if (ck_original) {
                    for (var field in VM.check) {
                        if (!field.match(/^\$\$.*$/)) {
                            if (VM.check[field] != ck_original[field]) {
                                QuestionsDic.registerChecksChange("Modified", VM.check.id, _testset.id, _questionwip);
                                break; // try to register only once :)
                            }
                        }
                    }
                }
            }
        }
    }

    function onTestsetSelected() {
        refresh();
    }

    function onCheckSelected() {
        refresh2();
    }

    function onCheckAdded() {
        UiStateManager.set("questions.checkSection", VM.sections[0].slug);
    }
}


})(angular);