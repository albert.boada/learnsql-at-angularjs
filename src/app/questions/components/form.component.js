(function (ng) { 'use strict';

questionsModule.component("questionForm", {
    controller  : QuestionFormCtrl, controllerAs: "VM",
    templateUrl : "questions/components/form.tpl.html"
});

QuestionFormCtrl.$inject = [
"Questions.QuestionsDic",
"Categories.CategoriesDic",
"Thematics.ThematicsDic",
"Schemas.SchemasDic",
"Questions.SolutiontypesDic",
"$state",
"$scope",
"Commons.UiStateManager",
"Commons.Utils",
"$translate",
"$rootScope"];
function QuestionFormCtrl(QuestionsDic, CategoriesDic, ThematicsDic, SchemasDic, SolutiontypesDic, $state, $scope, UiStateManager, Utils, $translate, $rootScope)
{
    var VM = this;
    ng.extend(VM, {
        /**
         * Model
         */
        question      : null,
        categories    : [],
        thematics     : [],
        schemas       : [],
        solutiontypes : [],
        totalWeight   : 0,
        correctionsOptions: [{key: null, val: "-"}, {key: true, val: "YES"}, {key: false, val: "NO"}],
        sections  : [
            {name: "GENERAL_INFO",   slug: "general"},
            {name: "SQL_STATEMENTS", slug: "sql"},
            {name: "TESTSETS",       slug: "testsets"},
            {name: "WEIGHTS",        slug: "weights"},
            {name: "SNAPSHOT",       slug: "snapshot"}
        ],
        scrollPos : 0,

        /**
         * Actions
         */
        openHtmlWindow: openHtmlWindow,
        viewAttachment: viewAttachment,
        shouldShowSection: shouldShowSection,
        shouldDisableElement: shouldDisableElement,
        showWording: showWording,
        shouldShowWording: shouldShowWording,
        previewCurrentWording: previewCurrentWording,
        onCategoryIdChanged: onCategoryIdChanged,
    });

    var _question, _category, _questionComponent;

    componentDidMount();

    /***************************************************************************
     * Implementation
     **************************************************************************/

    function componentDidMount() {
        refresh();

        VM.categories    = CategoriesDic.getAll();
        VM.schemas       = SchemasDic.getAll();
        VM.solutiontypes = SolutiontypesDic.getAll();

        // Set default visible form section
        UiStateManager.setDefault("questions.formsection", VM.sections[0].slug);

        // Set this very state (form) as the state that should be shown next time we navigate to a question
        UiStateManager.set("questions.substate", $state.current.name);
        UiStateManager.setDefault("questions.showWording", "ca");


        $scope.$on("questionSelected", onQuestionSelected);
        $scope.$on("doQuestionSave", onDoQuestionSave);
        $scope.$on("doQuestionRevert", onDoQuestionRevert);
        $scope.$watch(function () { return VM.question; }, onQuestionModified, true); // deep!!
    }

    function refresh() {
        _question = UiStateManager.get("questions.question");
        if (_question) {
            VM.question = QuestionsDic.getWip(_question);
            refreshCategory();
            refreshThematics();

            refresh2();
        }
    }

    function refresh2() {
        VM.totalWeight = 0;
        for (var i = 0; i < VM.question.juegosPruebas.length; i++) {
            VM.totalWeight += VM.question.juegosPruebas[i].peso;
        }
        VM.totalWeight = Math.round((VM.totalWeight * 100)) / 100;

        QuestionsDic.deregisterQuestionInfoModified(VM.question);

        if (QuestionsDic.hasChanges(_question, false)) {
            var diff = QuestionsDic.getDiff(_question);
            for (var field in diff) {
                if (field.match(/^juegosPruebas/) == null) {
                    QuestionsDic.registerQuestionInfoModified(VM.question);
                } else {
                    // @todo register specific Testset as modified only if WEIGHT has been modified
                }
            }
        }
    }


    function onQuestionSelected(e, data) {
        refresh();
    }

    function onQuestionModified(_new, _old) {
        if (_new && _old && _new.id == _old.id) {
            QuestionsDic.checkForChanges(_question);

            $scope.$emit("questionModified");

            refresh2();

            /*

            if (QuestionsDic.hasChanges(_question, false)) {
                var diff = QuestionsDic.getDiff(_question);

                //var check_for = ["inits", "solucion", "limpieza", "postinits", "postInitsJP", "juegosPruebas.*", "juegosPruebas.*.inits", "juegosPruebas.*.entrada", "juegosPruebas.*.limpieza", "juegosPruebas.*.comprobaciones.*", "juegosPruebas.*.comprobaciones.*.entrada"];
                var dif, key, depth, leaf;
                for (key in diff) {
                    dif = diff[key];
                    depth = dif.path.length;
                    leaf = dif.path[depth-1];

                    if (depth == 1 && ["inits", "solucion", "limpieza", "postinits", "postInitsJP"].indexOf(leaf) != -1) {
                        // console.log("question "+leaf+" modified");
                        do_unresolve = true;
                    }
                    else if (depth == 3 && dif.path[0] == "juegosPruebas" && ["inits", "entrada", "limpieza"].indexOf(leaf) != -1) {
                        // console.log("juegosPruebas "+leaf+" modified!!!");
                        do_unresolve = true;
                    }
                    else if (depth == 5 && dif.path[2] == "comprobaciones" && ["entrada"].indexOf(leaf) != -1) {
                        // console.log("comprobaciones "+leaf+" modified!!!");
                        do_unresolve = true;
                    }
                    else {
                        if (["N", "D"].indexOf(dif.kind) > -1) {
                            if ((depth == 2 && dif.path[depth-2] == "juegosPruebas") || (depth == 4 && dif.path[depth-2] == "comprobaciones")) {
                                // console.log("collections added/removed!!!");
                                do_unresolve = true;
                            }
                        }
                    }
                }
            }

            if (do_unresolve) {
                    VM.question.disponible = false;
                } else {
                VM.question.disponible = _question.disponible; // rollback to original
            }
            */
        }
    }

    function onDoQuestionSave() {
         if (confirm($translate.instant("CONFIRM_SAVE")+"\n\n"+getChangesSummary())) {
            $scope.$emit("doShowLoader", true);
            if (_question.id) {
                QuestionsDic.saveChanges(_question, onQuestionSaveSuccess, onQuestionSaveError);
            } else {
                QuestionsDic.saveNew(onQuestionSaveSuccess, onQuestionSaveError);
            }
        }

        function onQuestionSaveSuccess(response) {
            alert($translate.instant("SAVE_SUCCESS")+"\n\n:)");
            if (_question.id === 0) {
                var new_id = response.id;

                // Restructure tssindices and ckindices :)
                var tssindices = UiStateManager.get("questions.tssindices");
                var cksindices = UiStateManager.get("questions.cksindices");
                if (tssindices && !ng.isUndefined(tssindices[0])) {
                    tssindices[new_id] = tssindices[0];
                    delete tssindices[0];
                }
                if (cksindices && !ng.isUndefined(cksindices[0])) {
                    cksindices[new_id] = cksindices[0];
                    delete cksindices[0];
                }

                $scope.$emit("doGoToQuestion", new_id);
            }
            else {
                $rootScope.$broadcast("questionSelected");
            }

            $scope.$emit("doShowLoader", false);
        }
        function onQuestionSaveError(response) {
            if (ng.isDefined(response.validation)) {
                handleSaveValidationResponse(response.validation);
            }
            else {
                Utils.alertServerError(response);
                $scope.$emit("doShowLoader", false);
            }
        }

        function handleSaveValidationResponse(validation) {
            var error      = validation[0];
            var path       = validation[1];
            var field      = validation[2];
            var error_vars = validation[3];

            var on_done = function onDone() {
                Utils.alertValidationError(error, error_vars, path, field);
                $scope.$emit("doShowLoader", false);
            };

            if (error == "TESTSETSWEIGHTS") {
                UiStateManager.set("questions.formsection", "weights");
            }
            else if (!path) {
                var sql_fields = ["INITS", "SOLUTION", "URL", "USER", "PASSWORD", "SOLUTIONTYPE", "CLEANUP", "POSTINITS", "POSTINITSJP"];
                var formsection = "general";
                if (sql_fields.indexOf(field) != -1) {
                    formsection = "sql";
                } else if (field == "SNAPSHOT") {
                    formsection = snapshot;
                }
                UiStateManager.set("questions.formsection", formsection);
            }
            else if (path.match(/^juegosPruebas\.(\d+)/) != null) {
                var ts_index, ck_index;
                var testsetSection = "general";
                var checkSection   = "general";

                var matches = path.match(/^juegosPruebas\.(\d+)(?:\.comprobaciones\.(\d+))?$/);
                if (ng.isDefined(matches[2])) { // error belongs to Check
                    testsetSection = "checks";
                    ts_index = matches[1];
                    ck_index = matches[2];
                    var sql_fields3 = ["INPUT"];
                    if (sql_fields3.indexOf(field) != -1) {
                        checkSection = "sql";
                    }
                }
                else if (ng.isDefined(matches[1])) { // error belongs to Testset
                    ts_index = matches[1];
                    var sql_fields2 = ["INITS", "INPUT", "CLEANUP"];
                    if (sql_fields2.indexOf(field) != -1) {
                        testsetSection = "sql";
                    }
                }

                UiStateManager.set("questions.formsection", "testsets");
                UiStateManager.set("questions.testsetSection", testsetSection);
                if (testsetSection == "checks") {
                    UiStateManager.set("questions.checkSection", checkSection);
                    $scope.$broadcast("goToTestsetAndCheck", ts_index, ck_index, on_done);
                } else {
                    $scope.$broadcast("goToTestset", ts_index, on_done);
                }

                return;
            }

            Utils.execAfterCurrentCycle(on_done);
        }
    }



    function onDoQuestionRevert() {
        if (confirm($translate.instant("CONFIRM_REVERT")+"\n\n"+getChangesSummary())) {
            QuestionsDic.revertChanges(_question);
            $rootScope.$broadcast("questionSelected");
        }
    }

    function getChangesSummary() {
        var txts = [];
        var details = [];
        var flags = VM.question.$$$changesFlags;
        if (flags.question) {
            txts.push("- "+$translate.instant("CHANGED_QUESTION"));
        }
        if (flags.testsetsAdded.length || flags.testsetsModified.length || flags.testsetsRemoved.length) {
            if (flags.testsetsAdded.length) {
                details.push($translate.instant("XNEWM", {x: flags.testsetsAdded.length}));
            }
            if (flags.testsetsModified.length) {
                details.push($translate.instant("XMODIFIEDM", {x: flags.testsetsModified.length}));
            }
            if (flags.testsetsRemoved.length) {
                details.push($translate.instant("XREMOVEDM", {x: flags.testsetsRemoved.length}));
            }
            txts.push("- "+$translate.instant("CHANGED_TESTSETS", {summary: details.join(", ")}));
        }

        if (flags.checksAdded.length || flags.checksModified.length || flags.checksRemoved.length) {
            details = [];
            if (flags.checksAdded.length) {
                details.push($translate.instant("XNEWF", {x: flags.checksAdded.length}));
            }
            if (flags.checksModified.length) {
                details.push($translate.instant("XMODIFIEDF", {x: flags.checksModified.length}));
            }
            if (flags.checksRemoved.length) {
                details.push($translate.instant("XREMOVEDF", {x: flags.checksRemoved.length}));
            }
            txts.push("- "+$translate.instant("CHANGED_CHECKS", {summary: details.join(", ")}));
        }

        return txts.join("\n");
    }

    function onCategoryIdChanged() {
        refreshThematics();

        // From previous selected thematics, leave the ones that still belong
        // to the newly selected category, and delete the rest
        var new_thematics_ids = VM.thematics.map(function (t) { return t.id; });
        var i, t_id;
        for (i = VM.question.tematicasIds.length - 1; i >= 0; i--) {
            t_id = VM.question.tematicasIds[i];
            if (new_thematics_ids.indexOf(t_id) == -1) {
                VM.question.tematicasIds.splice(i, 1);
            }
        }

        refreshCategory();
        $scope.$broadcast("categoryIdChanged");
    }

    function refreshThematics() {
        if (!VM.question) { // this check should not be needed if a new question always had default fields
            VM.thematics = [];
        } else {
            VM.thematics = ThematicsDic.getByCategoryId(VM.question.categoriaId);
        }
    }

    function refreshCategory() {
        if (!VM.question) { // this check should not be needed if a new question always had default fields
            _category = null;
        } else {
            _category = CategoriesDic.getInstance(VM.question.categoriaId);
        }
    }

    function openHtmlWindow(html) {
        var myWindow = window.open("", "myWindow", "height=600,width=800");
        myWindow.document.write(html ? html : "");
    }

    function openTextWindow(text) {
        var myWindow = window.open("", "myWindow", "height=600,width=600");
        myWindow.document.write("<pre>"+text+"</pre>");
    }

    function viewAttachment() {
        if (!VM.question.extensionAdjunto) { return; }

        if (["txt", "sql"].indexOf(VM.question.extensionAdjunto) != -1) {
            openTextWindow(VM.question.ficheroAdjunto);
        } else {
            alert("@ToDo download file");
        }
    }

    function shouldShowSection(slug) {
        return UiStateManager.get("questions.formsection") == slug;
    }

    function shouldDisableElement(slug) {
        switch (slug) {
            case "snapshot":
                return !_category || !!_category.estado; // Logic is reversed on purpose. Checked Category features are the ones that are NOT allowed.
            case "inits":
                return !_category || !!_category.inits;
            case "postinits":
                return !_category || !!_category.postinits;
            case "solution":
                return !_category || !!_category.solucion;
            case "cleanup":
                return !_category || !!_category.limpieza;
        }

        return false;
    }

    function shouldShowWording(lang) {
        return UiStateManager.get("questions.showWording") == lang;
    }

    function showWording(lang) {
        UiStateManager.set("questions.showWording", lang);
    }

    function previewCurrentWording() {
        var lang = UiStateManager.get("questions.showWording");
        var field = "enunciado";
        switch (lang) {
            case 'es': field += "Esp"; break;
            case 'en': field += "Ing"; break;
        }
        openHtmlWindow(VM.question[field]);
    }

    /*function changeSection(slug) {
        // UiStateManager.set("questions.scrolls.question", 0);
        VM.scrollPos = 0;
        UiStateManager.set("questions.formsection", slug);
    }*/
}

})(angular);
