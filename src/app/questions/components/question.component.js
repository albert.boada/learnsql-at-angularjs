(function (ng) { 'use strict';

questionsModule.component("question", {
    controller: QuestionCtrl, controllerAs: "VM",
    templateUrl: "questions/components/question.tpl.html"
});

QuestionCtrl.$inject = [
"Questions.QuestionsDic",
"Commons.UiStateManager",
"Commons.Utils",
"$scope",
"$rootScope",
"$state",
"$translate"];
function QuestionCtrl(QuestionsDic, UiStateManager, Utils, $scope, $rootScope, $state, $translate)
{
    var VM = this;
    ng.extend(VM, {
        /**
         * Model
         */
        question: null,
        isNew: false,
        isDirty: false,
        mood: null,

        /**
         * Actions
         */
        shouldShowSection: shouldShowSection,
        getField: getField,
        saveChanges: saveChanges,
        revertChanges: revertChanges,
        remove: remove,
        lock: lock,
        copyForNew: copyForNew,
        copyToDb: copyToDb,
        generateList: generateList,
    });

    componentDidMount();

    /***************************************************************************
     * Implementation
     **************************************************************************/

    function componentDidMount() {
        refresh();

        $scope.$on("questionSelected", onQuestionSelected);
        $scope.$on("questionModified", onQuestionModified);
        // $scope.$watch(function () { return vm.question.titulo; }, onTitleChanged);
    }

    function shouldShowSection(slug) {
        switch (slug) {
            case "form":
                return $state.includes("questionForm", {id: VM.question.id});
            case "execute":
                return $state.includes('questionExecute', {id: VM.question.id});
        }

        return false;
    }

    function getField(field) {
        if (VM.question) {
            return QuestionsDic.getWipValueOrFallback(VM.question, field);
        }
        return "";
    }

    function saveChanges() {
        $scope.$broadcast("doQuestionSave");
    }

    function revertChanges() {
        $scope.$broadcast("doQuestionRevert");
    }

    function remove() {
        if (confirm($translate.instant("CONFIRM_REMOVE"))) {
            $scope.$emit("doShowLoader", true);
            QuestionsDic.remove(VM.question, onQuestionRemovedSuccess, onQuestionRemovedError);
        }

        function onQuestionRemovedSuccess() {
            $state.go("questions").then(function () {
                alert($translate.instant("REMOVED_ENTITY")+"\n\n:)");
                $scope.$emit("doShowLoader", false);
            });
        }

        function onQuestionRemovedError(response) {
            Utils.alertServerError(response);
            $scope.$emit("doShowLoader", false);
        }
    }

    function lock(lock) {
        var confirm_msg = lock ? "CONFIRM_LOCK" : "CONFIRM_UNLOCK";

        if (confirm($translate.instant(confirm_msg))) {
            $scope.$emit("doShowLoader", true);
            QuestionsDic.lock(VM.question, onQuestionLockedSuccess, onQuestionLockedError);
        }

        function onQuestionLockedSuccess(response) {
            var msg = lock ? "LOCK_SUCCESS" : "UNLOCK_SUCCESS";

            alert($translate.instant(msg)+"\n\n:)");

            $rootScope.$broadcast("questionSelected");
            $scope.$emit("doShowLoader", false);
        }

        function onQuestionLockedError(response) {
            Utils.alertServerError(response);
            $scope.$emit("doShowLoader", false);
        }
    }

    function copyForNew() {
        if (confirm($translate.instant("CONFIRM_COPY"))) {
            $scope.$emit("doShowLoader", true);
            QuestionsDic.copy(VM.question, onQuestionCopiedSuccess, onQuestionCopiedError);
        }

        function onQuestionCopiedSuccess(response) {
            alert($translate.instant("COPY_SUCCESS")+"\n\n:)");
            $scope.$emit("doGoToQuestion", response.id);
            $scope.$emit("doShowLoader", false);
        }

        function onQuestionCopiedError(response) {
            Utils.alertServerError(response);
            $scope.$emit("doShowLoader", false);
        }
    }

    function copyToDb() {
        alert("@todo");
    }

    function generateList() {
        alert("@todo");
    }


    function refresh() {
        VM.question = UiStateManager.get("questions.question");
        if (VM.question) {
            VM.isNew = (VM.question.id === 0);

            refresh2();
        }
    }

    function refresh2() {
        VM.isDirty = QuestionsDic.hasChanges(VM.question, false);
        refreshMood();
    }

    function refreshMood() {
        VM.mood = "neutral";
        if (VM.isNew) {
            VM.mood = "primary";
        } else if (VM.isDirty) {
            VM.mood = "warning";
        } else if (VM.question.disponible) {
            VM.mood = "success";
        }
    }

    function onQuestionSelected(e, data) {
        refresh();
    }

    function onQuestionModified(e) {
        refresh2();
    }

    /*function onTitleChanged() {
        VM.title = makeViewTitle(VM.question);
    }

    function scrollToBottom() {
        $location.hash("bottom");
        $anchorScroll();
    }

    function gotobottom() {
        $location.hash("bottom");
        $anchorScroll();
    }

    function makeViewTitle(question) {
        var title = "Nova qüestió";
        if (question !== null) {
            title = sprintf("Qüestió: %s", question.titulo);
        }
        return title;
    }*/
}


})(angular);