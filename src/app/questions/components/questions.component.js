(function (ng) { 'use strict';

questionsModule.component("questions", {
    controller: QuestionsCtrl, controllerAs: "VM",
    templateUrl: "questions/components/questions.tpl.html"
});

QuestionsCtrl.$inject = [
"Questions.QuestionsDic",
"Commons.UiStateManager",
"Commons.Utils",
"$scope",
"$state",
"$stateParams"];
function QuestionsCtrl(QuestionsDic, UiStateManager, Utils, $scope, $state, $stateParams)
{
    var VM = this;
    ng.extend(VM, {
        /**
         * Model
         */
        questions: [],

        /**
         * Actions
         */
        pick: pick,
        addNew: addNew,
    });

    var _current;

    componentDidMount();

    /***************************************************************************
     * Implementation
     **************************************************************************/

    function componentDidMount() {
        VM.questions = QuestionsDic.getAll();

        refresh();

        $scope.$on("doNewQuestion", onDoNewQuestion);
        $scope.$on("doGoToQuestion", onDoGoToQuestion);

        $scope.$watch(function () { return $stateParams.id; }, onUrlIdChanged);
    }

    function refresh() {
        var q;
        if (ng.isDefined($stateParams.id)) {
            if (Utils.isCreateRouteKeyword($stateParams.id)) {
                q = QuestionsDic.new_entity;
            } else {
                q = QuestionsDic.getInstance($stateParams.id);
            }

            if (!q) {
                $state.go("questions").then(function () { alert("q not found"); });
                return;
            }
        }
        setCurrent(q);
    }

    function pick(id) {
        var is_create = Utils.isCreateRouteKeyword(id);

        var transitionTo = {name: "questionForm", params: {"id": id}};

        if (!is_create && UiStateManager.exists("questions.substate")) {
            transitionTo.name = UiStateManager.get("questions.substate");
        }

        if ($state.is("questions")) {
            $state.go(transitionTo.name, transitionTo.params);
            return;
        }

        if (is_create && !$state.is("questionForm")) {
            $state.go("questionForm").then(function () {
                __go();
            });
            return;
        }

        __go();

        function __go() {
            Utils.goWithoutReload(transitionTo, "mainOutlet@root", 1);
        }
    }

    function addNew() {
        pick("new");
    }

    function onUrlIdChanged(_new, _old) {
        if ($state.includes("questions") && _new != _old) {
            refresh();
        }
    }

    function onDoNewQuestion() {
        addNew();
    }

    function onDoGoToQuestion(e, id) {
        pick(id);
    }

    function setCurrent(question) {
        _current = question;
        broadcast(question);
    }

    function broadcast(question) {
        UiStateManager.set("questions.question", question);
        $scope.$broadcast("questionSelected");
    }
}

})(angular);