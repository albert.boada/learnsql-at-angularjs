(function (ng) { 'use strict';

questionsModule.component("questionExecute", {
    controller  : QuestionExecuteCtrl, controllerAs: "VM",
    templateUrl : "questions/components/execute.tpl.html"
});

QuestionExecuteCtrl.$inject = [
"$scope",
"$state",
"Commons.UiStateManager"];
function QuestionExecuteCtrl($scope, $state, UiStateManager)
{
    componentDidMount();

    /***************************************************************************
     * Implementation
     **************************************************************************/

    function componentDidMount() {
        UiStateManager.set("questions.substate", $state.current.name); //... same code as in formctrl... @todo generalise??
    }
}

})(angular);
