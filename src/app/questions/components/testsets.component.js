(function (ng) { 'use strict';

questionsModule.component("questionTestsets", {
    controller: QuestionTestsetsCtrl, controllerAs: "VM",
    templateUrl: "questions/components/testsets.tpl.html"
});

QuestionTestsetsCtrl.$inject = [
"Questions.QuestionsDic",
"Questions.TestsetsService",
"Commons.UiStateManager",
"Commons.Utils",
"$scope"];
function QuestionTestsetsCtrl(QuestionsDic, TestsetsService, UiStateManager, Utils, $scope)
{
    var VM = this;
    ng.extend(VM, {
        /**
         * Model
         */
        testsets : [],
        num_total: 0,

        /**
         * Actions
         */
        pick      : pick,
        isCurrent : isCurrent,
        addNew    : addNew,
        canRemove : canRemove,
        removeCurrent : removeCurrent,
    });

    var _current, _questionwip;

    componentDidMount();

    /***************************************************************************
     * Implementation
     **************************************************************************/

    function componentDidMount() {
        refresh();

        // $scope.$watch(function () { return _questionwip; }, onQuestionSelected);
        $scope.$watch(function () { return VM.testsets.length; }, onTestsetsLengthChanged);
        $scope.$on("questionSelected", onQuestionSelected);
        $scope.$on("doDeleteCurrentTestset", onDoDeleteCurrentTestset);
        $scope.$on("goToTestset", onGoTo);
        $scope.$on("goToTestsetAndCheck", onGoToTestsetAndCheck);
    }

    function refresh()  {
        var q = UiStateManager.get("questions.question");
        if (q) {
            _questionwip = QuestionsDic.getWip(q);
            var index = null;
            VM.testsets = {};
            if (_questionwip) {
                VM.testsets = _questionwip.juegosPruebas;
                index = obtainCurrent();
            }
            setCurrent(index);

            VM.num_total = VM.testsets.length;
        }
    }

    function obtainCurrent() {
        var index = UiStateManager.get("questions.tssindices."+_questionwip.id);
        /*if (ng.isUndefined(index)) {
            index = 0;
        }*/

        if (ng.isUndefined(VM.testsets[index])) {
            return null;
        }

        return index;
    }

    function onQuestionSelected(e, data) {
        refresh();
    }

    function onDoDeleteCurrentTestset(e) {
        removeCurrent();
    }

    function onTestsetsLengthChanged(_new, _old) {
        if (_new != _old) {
            VM.num_total = _new;
        }
    }

    function onGoTo(e, index, callback) {
        pick(index);
        Utils.execAfterCurrentCycle(callback);
    }

    function onGoToTestsetAndCheck(e, ts_index, ck_index, callback) {
        pick(ts_index);
        Utils.execAfterCurrentCycle(function () {
            $scope.$broadcast("goToCheck", ck_index, callback);
        });
    }

    function pick(index) {
        setCurrent(index);
    }

    function isCurrent(index) {
        return _current == index;
    }

    function addNew() {
        var ts =
        TestsetsService.addNew("Nou JP (#{{counter}})", VM.testsets);
        setCurrent(VM.testsets.length - 1);

        QuestionsDic.registerTestsetsChange("Added", ts.id, _questionwip);

        $scope.$broadcast("testsetAdded");
    }

    function canRemove() {
        return _current != null;
    }

    function removeCurrent() {
        deleteTestset(_current);
    }

    function deleteTestset(index) {
        var ts_deleted =
        TestsetsService.deleteByIndex(index, VM.testsets);

        // Update default name new instances
        for (var i = index; i < VM.testsets.length; i++) {
            var ts = VM.testsets[i];
            if (ts.nombre.match(/Nou JP \(#[\d]+\)/)) {
                ts.nombre = "Nou JP (#"+(i+1)+")";
            }
        }

        // Update memorised selected Testset indices
        // ^ NOT NEEDED, because the user is forced to pick one again manually, so it will get "self-corrected" by this action anyway

        // Update memorised selected Cheks indices
        var cksindices = UiStateManager.get("questions.cksindices."+_questionwip.id);
        for (var ts_index in cksindices) {
            if (ts_index < index) {
                continue;
            } else if (ts_index == index) {
                delete cksindices[ts_index];
            } else {
                cksindices[ts_index - 1] = cksindices[ts_index];
                delete cksindices[ts_index];
            }
        }

        QuestionsDic.registerTestsetsChange("Removed", ts_deleted.id, _questionwip);

        setCurrent(null);
    }

    function setCurrent(index) {
        _current = index;
        UiStateManager.set("questions.tssindices."+_questionwip.id, index);
        broadcast(index);
    }

    function broadcast(index) {
        UiStateManager.set("questions.testsetIndex", index); // Make it available globally (for all child components)
        $scope.$broadcast("testsetSelected");
    }
}


})(angular);