(function (ng) { 'use strict';

questionsModule.component("questionTestset", {
    controller: QuestionTestsetCtrl, controllerAs: "VM",
    templateUrl: "questions/components/testset.tpl.html"
});

QuestionTestsetCtrl.$inject = [
"Questions.QuestionsDic",
"Categories.CategoriesDic",
"Commons.UiStateManager",
"$scope"];
function QuestionTestsetCtrl(QuestionsDic, CategoriesDic, UiStateManager, $scope)
{
    var VM = this;
    ng.extend(VM, {
        /**
         * Model
         */
        testset  : null,
        sections : [
            {name: "GENERAL_INFO", slug: "general"},
            {name: "SQL_STATEMENTS", slug: "sql"},
            {name: "CHECKS", slug: "checks"}
        ],
        booleanOptions: [{key: null, val: "-"}, {key: true, val: "YES"}, {key: false, val: "NO"}],

        /**
         * Actions
         */
        remove: remove,
        shouldShowSection: shouldShowSection,
        shouldDisableElement: shouldDisableElement,
        showErrormsg: showErrormsg,
        shouldShowErrormsg: shouldShowErrormsg,
    });

    var _question, _questionwip, _category;

    componentDidMount();

    /***************************************************************************
     * Implementation
     **************************************************************************/

    function componentDidMount() {
        refresh();

        // Set default visible section
        UiStateManager.setDefault("questions.testsetSection", VM.sections[0].slug); // @todo move to state on successful navigation?

        UiStateManager.setDefault("questions.showTestsetErrormsg", "ca"); // @todo move to state on successful navigation?


        $scope.$on("questionSelected", onQuestionSelected);
        $scope.$on("categoryIdChanged", onCategoryIdChanged);
        $scope.$on("testsetSelected", onTestsetSelected);

        $scope.$on("testsetAdded", onTestsetAdded);
        $scope.$watch(function () { return VM.testset; }, onTestsetModified, true); // true!!
    }

    function remove() {
        $scope.$emit("doDeleteCurrentTestset"); // up!
    }

    function shouldShowSection(slug) {
        return UiStateManager.get("questions.testsetSection") == slug;
    }

    function shouldDisableElement(slug) {
        switch (slug) {
            case "checks":
                return !_category || !!_category.comprobaciones; // Logic is reversed on purpose. Checked Category features are the ones that are NOT allowed.
            case "inits":
                return !_category || !!_category.initsJP;
            case "input":
                return !_category || !!_category.entradaJP;
            case "cleanup":
                return !_category || !!_category.limpiezaJP;
        }

        return false;
    }

    function shouldShowErrormsg(lang) {
        return UiStateManager.get("questions.showTestsetErrormsg") == lang;
    }

    function showErrormsg(lang) {
        UiStateManager.set("questions.showTestsetErrormsg", lang);
    }

    function refresh() {
        _question = UiStateManager.get("questions.question");
        if (_question) {
            _questionwip = QuestionsDic.getWip(_question);
            refreshCategory();
            refresh2();
        }
    }

    function refresh2() {
        VM.testset = null;
        if (_questionwip) {
            VM.testset = QuestionsDic.findTestsetByIndex(UiStateManager.get("questions.testsetIndex"), _questionwip);
        }
    }

    function refreshCategory() {
        _category = null;
        if (_questionwip) {
            _category = CategoriesDic.getInstance(_questionwip.categoriaId);
        }
    }

    function onTestsetModified(_new, _old) {
        if (_new && _old && _new.id == _old.id) {

            QuestionsDic.deregisterTestsetsChange("Modified", VM.testset.id, _questionwip); // reset before checking again

            var ts_original = QuestionsDic.findTestsetById(VM.testset.id, _question);
            if (ts_original) {
                for (var field in VM.testset) {
                    if (!field.match(/^comprobaciones|\$\$.*$/)) {
                        if (VM.testset[field] != ts_original[field]) {
                            QuestionsDic.registerTestsetsChange("Modified", VM.testset.id, _questionwip);
                            break; // try to register only once :)
                        }
                    }
                }
            }
        }
    }

    function onTestsetAdded() {
        UiStateManager.set("questions.testsetSection", VM.sections[0].slug);
    }

    function onQuestionSelected() {
        refresh();
    }

    function onTestsetSelected() {
        refresh2();
    }

    function onCategoryIdChanged() {
        refreshCategory();
    }

}


})(angular);