var questionsModule;

(function (ng) { 'use strict';

var dependencies = [];

questionsModule = ng.module(appName+".Questions", dependencies);
questionsModule.config(questionsModuleConfig);
questionsModule.run(questionsModuleRun);

function questionsModuleConfig() {}

questionsModuleRun.$inject = [];
function questionsModuleRun() {}

})(angular);