(function (ng) { 'use strict';

questionsModule.factory("Questions.ChecksService", ChecksServiceFactory);

ChecksServiceFactory.$inject = [];
function ChecksServiceFactory()
{
    var counter_new = 0;

    class ChecksService
    {
        findByIndex(index, checks) {
            if (typeof checks[index] != "undefined") {
                return checks[index];
            }

            return null;
        }

        deleteByIndex(index, checks) {
            var ck = !ng.isUndefined(checks[index]) ? checks[index] : null;

            checks.splice(index, 1);

            return ck;
        }

        findById(id, checks) {
            if (!this.has(checks)) { return null; }

            var ck;
            for (var i = 0; i < checks.length; i++) {
                ck = checks[i];
                if (ck.id == id) {
                    return ck;
                }
            }

            return null;
        }

        addNew(name, checks) {
            counter_new++;
            var check = {
                // $$$new: true,
                id: "$$$new"+counter_new,
                nombre: name,
                descripcion: "",
                errorMsg: "",
                binaria: null,
                consumeix: null,
            };
            checks.push(check);

            check.nombre = check.nombre.replace("{{counter}}", checks.length);

            return check;
        }

        has(checks) {
            return Array.isArray(checks) && checks.length > 0;
        }
    }

    return new ChecksService();
}

})(angular);