(function (ng) { 'use strict';

questionsModule.factory("Questions.TestsetsService", TestsetsServiceFactory);

TestsetsServiceFactory.$inject = ["Questions.ChecksService"];
function TestsetsServiceFactory(ChecksService)
{
    var counter_new = 0;

    class TestsetsService
    {
        findByIndex(index, testsets) {
            if (typeof testsets[index] != "undefined") {
                return testsets[index];
            }

            return null;
        }

        deleteByIndex(index, testsets) {
            var ts = !ng.isUndefined(testsets[index]) ? testsets[index] : null;

            testsets.splice(index, 1);

            return ts;
        }

        findById(id, testsets) {
            if (!this.has(testsets)) { return null; }

            var ts;
            for (var i = 0; i < testsets.length; i++) {
                ts = testsets[i];
                if (ts.id == id) {
                    return ts;
                }
            }

            return null;
        }

        addNew(name, testsets) {
            counter_new++;
            var testset = {
                id: "$$$new"+counter_new,
                nombre: name,
                descripcion: "",
                msgError: "",
                peso: 0,
                binaria: null,
                mensaje: null,
                consumeix: null,
                comprobaciones: [],
            };
            testsets.push(testset);

            testset.nombre = testset.nombre.replace("{{counter}}", testsets.length);

            return testset;
        }

        has(testsets) {
            return Array.isArray(testsets) && testsets.length > 0;
        }

        findCheckByIndex(index, testset) {
            return ChecksService.findByIndex(index, testset.comprobaciones);
        }

        findCheckById(id, testset) {
            return ChecksService.findById(id, testset.comprobaciones);
        }

        addCheck(testset) {
            return ChecksService.addNew(testset.comprobaciones);
        }

        hasChecks(testset) {
            return ChecksService.has(testset.comprobaciones);
        }
    }

    return new TestsetsService();

}

})(angular);