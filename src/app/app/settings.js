(function () { 'use strict';

var appSettings = {
    "defaultUrl"         : "/questions",
    // "backendBaseUrl"     : "http://localhost:8080/learnsql-at/ws", // Dev
    "backendBaseUrl"     : "ws", // Prod
    "routeCreateKeyword" : "new"
};

appModule.constant("appSettings", appSettings);

})();