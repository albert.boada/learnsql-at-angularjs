var appName = "LearnSqlApp",
    APP     = {"States": {}, "FilterComparators": {}}, // App's namespace for POJOs, so there are no name collisions
    appModule;

(function () { 'use strict';

var dependencies = [
    "templates-app",
    "templates-common",

    "ui.router", // angular UI router
    "ui.router.stateHelper", // for nested tree routes
    "ui.router.default", // for default child route
    //"ui.tinymce",

    "restangular",

    "frapontillo.ex.filters",

    "pascalprecht.translate",

    appName+".Commons",
    appName+".Questions",
    appName+".Categories",
    appName+".Thematics",
    appName+".Schemas",
];

appModule = angular.module(appName, dependencies);

})();