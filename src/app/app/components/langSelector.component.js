(function (ng) { 'use strict';

appModule.component("langSelector", {
    controller: LangSelectorCtrl, controllerAs: "LangCtrl",
    /* jshint ignore:start */
    template: `

        <div class="form-group">
            <select class="form-control input-sm"
                    ng-options="l.locale as l.name for l in LangCtrl.locales"
                    ng-model="LangCtrl.currentLocale"
                    ng-change="LangCtrl.change()"
            ></select>
        </div>

    `
    /* jshint ignore:end */
});

LangSelectorCtrl.$inject = ["$scope", "$translate"];
function LangSelectorCtrl($scope, $translate)
{
    var VM = this;
    ng.extend(VM, {
        /**
         * Model
         */
        currentLocale: null,
        locales: [
            {locale: "ca", name: "Català"},
            {locale: "es", name: "Castellano"},
            {locale: "en", name: "English"},
        ],

        /**
         * Actions
         */
        change: change,
    });

    componentDidMount();

    /***************************************************************************
     * Implementation
     **************************************************************************/

    function componentDidMount() {
        VM.currentLocale = $translate.use();
    }

    function change() {
        $translate.use(VM.currentLocale);
    }
}

})(angular);
