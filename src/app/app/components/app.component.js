(function (ng) { 'use strict';

appModule.component("app", {
    controller: AppCtrl, controllerAs: "App",
    templateUrl: "app/components/app.tpl.html"
});

AppCtrl.$inject = ["$scope", "$state", "Commons.UiStateManager", "Commons.Utils"];
function AppCtrl($scope, $state, UiStateManager, Utils)
{
    var VM = this;
    ng.extend(VM, {
        /**
         * Model
         */
        shouldShowLoader: false,
        tinymce: {
            theme: "modern"
        },

        /**
         * Actions
         */
        goTo: goTo,
        isCurrent: isCurrent,
    });

    componentDidMount();

    /***************************************************************************
     * Implementation
     **************************************************************************/

    function componentDidMount() {
        $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
            showLoader(true);
        });
        $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
            showLoader(false);

            if (ng.isDefined(toState.data) && ng.isDefined(toState.data.pageTitle)) {
                $scope.pageTitle = toState.data.pageTitle + ' | '+appName;
            }
        });
        $scope.$on("doShowLoader", function (e, show) { showLoader(show); });
    }

    function showLoader(show) {
        VM.shouldShowLoader = show;
    }

    function goTo(section) {
        var memo, memo2,
            state = section,
            params = null
        ;
        switch (section) {
            case "questions":
                memo = UiStateManager.get("questions.question.id");
                if (memo) {
                    state = UiStateManager.get("questions.substate");
                    state = state ? state : "question";
                    params = {id: memo};
                }
                break;
            case "categories":
                memo = UiStateManager.get("categories.category.id");
                if (memo) {
                    state = "category";
                    params = {id: memo};
                }
                break;
            case "thematics":
                memo = UiStateManager.get("thematics.thematic.id");
                if (memo) {
                    state = "thematic";
                    params = {id: memo};
                }
                break;
            case "schemas":
                memo = UiStateManager.get("schemas.schema.id");
                if (memo) {
                    state = "schema";
                    params = {id: memo};
                }
                break;
        }

        $state.go(state, params);
    }

    function isCurrent(section) {
        return $state.includes(section);
    }
}

})(angular);
