(function () { 'use strict';

var regexs = {
    "id" : "[0-9]+|new"
};

var statesurls = {

// Questions
"questions"       : ["/questions",              {}],
"questionForm"    : ["/questions/{id}",         {/*"id": regexs["id"]*/}],
"questionExecute" : ["/questions/{id}/execute", {/*"id": regexs["id"]*/}],

// Categories
"categories" : ["/categories", {}],
"categoryForm" : ["/categories/{id}", {/*"id": regexs["id"]*/}],

// Categories
"thematics" : ["/thematics", {}],
"thematicForm" : ["/thematics/{id}", {/*"id": regexs["id"]*/}],

// Categories
"schemas" : ["/schemas", {}],
"schemaForm" : ["/schemas/{id}", {/*"id": regexs["id"]*/}],

};

appModule.constant("statesurls", statesurls);

})();