(function (ng) { 'use strict';

appModule.config(setAppRoutes);

/* @ngInject */
function setAppRoutes($urlRouterProvider, stateHelperProvider, statesurls, appSettings, $urlMatcherFactoryProvider, $locationProvider) {
    $locationProvider.html5Mode(true);

    /**
     * Redirects redirects
     */
    $urlRouterProvider.when("",  appSettings.defaultUrl);
    $urlRouterProvider.when("/", appSettings.defaultUrl);

    $urlMatcherFactoryProvider.strictMode(false);

    __hookStates();

    /**
     *
     */
    function __hookStates() {
        angular.forEach(APP.States, function (appstate, i) {
            __hookState(appstate);
        });
    }

    /**
     *
     */
    function __hookState(state, is_child) {
        is_child = ng.isUndefined(is_child) ? false : is_child;

        // Hook state with its URL, if set by the developer
        var name = state.name;
        if (typeof statesurls[name] !== "undefined") {
            var url    = statesurls[name][0],
                params = statesurls[name][1];
            if (params && ng.isObject(params)) {
                ng.forEach(params, function (regex, param_name) {
                    url = url.replace(param_name, param_name+":"+regex);
                });
            }

            if (url) { url = "^"+url; }
            state.url = url;
        }

        if (!ng.isUndefined(state['children'])) {
            ng.forEach(state.children, function (childstate, i) {
                __hookState(childstate, true);
            });
        }


        if (!is_child) {
            // Hook state, finally!
            stateHelperProvider.state(state, { keepOriginalNames: true });
        }
    }
}

})(angular);

