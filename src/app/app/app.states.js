(function (States) { 'use strict';

States.app =

    {   name     : "root",
        abstract : true,
        resolve  : {
            dummyname1 : resolveQuestions,
            dummyname2 : resolveCategories,
            dummyname3 : resolveSchemas,
            dummyname4 : resolveThematics,
            dummyname5 : resolveSolutiontypes,
        },
        views    : {
            "rootOutlet": {
                template: `
                    <OUTLET ui-view="mainOutlet"
                            autoscroll="false"
                            class="flex flex-layout"
                    ></OUTLET>
                `
    }}}
;

resolveQuestions.$inject = ["Commons.Utils", "Questions.QuestionsDic"];
function resolveQuestions(Utils, QuestionsDic) {
    return Utils.resolveEntitiesList(QuestionsDic);
}

resolveCategories.$inject = ["Commons.Utils", "Categories.CategoriesDic"];
function resolveCategories(Utils, CategoriesDic) {
    return Utils.resolveEntitiesList(CategoriesDic);
}

resolveSchemas.$inject = ["Commons.Utils", "Schemas.SchemasDic"];
function resolveSchemas(Utils, SchemasDic) {
    return Utils.resolveEntitiesList(SchemasDic);
}

resolveThematics.$inject = ["Commons.Utils", "Thematics.ThematicsDic"];
function resolveThematics(Utils, ThematicsDic) {
    return Utils.resolveEntitiesList(ThematicsDic);
}

resolveSolutiontypes.$inject = ["Commons.Utils", "Questions.SolutiontypesDic"];
function resolveSolutiontypes(Utils, SolutiontypesDic) {
    return Utils.resolveEntitiesList(SolutiontypesDic);
}

})(APP.States);
