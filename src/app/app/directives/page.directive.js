(function (ng) { 'use strict';

appModule.directive("page", function () {
    return {
        restrict: 'A',
        scope: true,
        controller: PageCtrl, controllerAs: "Page"
    };
});

PageCtrl.$inject = [];
function PageCtrl()
{
    var VM = this;
    ng.extend(VM, {
        /**
         * Model
         */
        title: "A.T. (LearnSQL)",
    });
}


})(angular);