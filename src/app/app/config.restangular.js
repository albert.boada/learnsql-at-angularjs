(function () { 'use strict';

appModule.config(configureRestangular);
appModule.run(runRestangular);

/* @ngInject */
function configureRestangular(RestangularProvider, appSettings) {
    RestangularProvider.setBaseUrl(appSettings.backendBaseUrl);
    // restangularProvider.setRequestSuffix('.json');
    RestangularProvider.setFullResponse(true);
}

function runRestangular(Restangular, $translate) {
    Restangular.setErrorInterceptor(function(response, deferred, responseHandler) {
        if(response.status === 401) {
            alert($translate.instant("SESSION_EXPIRED"));
            window.location = "logout";

            return false; // error handled
        }

        return true; // error not handled
    });
}

})();