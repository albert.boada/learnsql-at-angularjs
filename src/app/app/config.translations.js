(function () { 'use strict';

appModule.config(configureTranslations);

/* @ngInject */
function configureTranslations($translateProvider, translations) {
    var en, es, ca;
    en = {};
    es = {};
    ca = {};

    var translation;
    for (var key in translations) {
        if (translations.hasOwnProperty(key)) {
            translation = translations[key];
            if (typeof translation[0] != "undefined") { en[key] = translation[0]; }
            if (typeof translation[1] != "undefined") { es[key] = translation[1]; }
            if (typeof translation[2] != "undefined") { ca[key] = translation[2]; }
        }
    }

    $translateProvider
        .translations("en", en)
        .translations("es", es)
        .translations("ca", ca)
        .preferredLanguage('ca')
        .fallbackLanguage('ca')
    ;
}

})();