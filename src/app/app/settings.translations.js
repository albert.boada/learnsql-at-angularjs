(function () { 'use strict';

var translations = {
    QUESTIONS: [
        "Questions",
        "Cuestiones",
        "Qüestions"
    ],
    CATEGORY: [
        "Category",
        "Categoría",
        "Categoria"
    ],
    CATEGORIES: [
        "Categories",
        "Categorías",
        "Categories"
    ],
    THEMATICS: [
        "Thematics",
        "Temáticas",
        "Temàtiques"
    ],
    SCHEMA: [
        "Schema",
        "Esquema",
        "Esquema"
    ],
    SCHEMAS: [
        "Schemas",
        "Esquemas",
        "Esquemes"
    ],
    SOLUTIONTYPE: [
        "Solution type",
        "Tipo Solución",
        "Tipus Solució"
    ],
    QUESTION_DATA: [
        "Question data",
        "Datos de Cuestión",
        "Dades de Qüestió"
    ],
    EXECUTION_PANEL: [
        "Execution panel",
        "Panel de ejecución",
        "Panell d'execució"
    ],
    GENERAL_INFO: [
        "General info",
        "Datos generales",
        "Dades generals"
    ],
    WORDING_INFO: [
        "Wording info",
        "Datos del enunciado",
        "Dades de l'enunciat"
    ],
    TESTSETS: [
        "Test sets",
        "Juegos de pruebas",
        "Jocs de proves"
    ],
    TESTSET: [
        "Test set",
        "Juego de pruebas",
        "Joc de proves"
    ],
    SQL_STATEMENTS: [
        "SQL statements",
        "Sentencias SQL",
        "Sentències SQL"
    ],
    INITS: [
        "Initialisations",
        "Inicializaciones",
        "Inicialitzacions"
    ],
    POSTINITS: [
        "Post initialisations",
        "Post inicializaciones",
        "Post inicialitzacions"
    ],
    SOLUTION: [
        "Solution",
        "Solución",
        "Solució"
    ],
    CLEANUP: [
        "Cleanup",
        "Limpieza",
        "Neteja"
    ],
    INPUT: [
        "Input",
        "Entrada",
        "Entrada"
    ],
    OUTPUT: [
        "Output",
        "Salida",
        "Sortida"
    ],
    SNAPSHOT: [
        "Snapshot",
        "Estado BD",
        "Estat BD"
    ],
    TITLE: [
        "Title",
        "Título",
        "Títol"
    ],
    NAME: [
        "Name",
        "Nombre",
        "Nom"
    ],
    DESCRIPTION: [
        "Description",
        "Descripción",
        "Descripció"
    ],
    ATTACHMENT: [
        "Attachment",
        "Fichero adjunto",
        "Fitxer adjunt"
    ],
    AUTHOR: [
        "Author",
        "Autor",
        "Autor"
    ],
    DIFFICULTY: [
        "Difficulty",
        "Dificultad",
        "Dificultat"
    ],
    BINARYCORR: [
        "Binary correction",
        "Corrección Binaria",
        "Correcció Binària"
    ],
    CELLCORR: [
        "Cell correction",
        "Correción Jaula",
        "Correció Gàbia"
    ],
    MSGCORR: [
        "Message correction",
        "Corrección Mensaje",
        "Correcció Missatge",
    ],
    CONSUMES_ATTEMPT: [
        "Consumes attempt",
        "Consume intento",
        "Consumeix intent",
    ],
    STATUS: [
        "Status",
        "Estado",
        "Estat"
    ],
    WORDING: [
        "Wording",
        "Enunciado",
        "Enunciat"
    ],
    HASFILE: [
        "Has a .{{ext}} file",
        "Tiene un fichero .{{ext}}",
        "Té un fitxer .{{ext}}"
    ],
    WEIGHTS: [
        "Weights",
        "Pesos",
        "Pesos"
    ],
    WEIGHT: [
        "Weight",
        "Peso",
        "Pes"
    ],
    CHECKS: [
        "Checks",
        "Comprobaciones",
        "Comprovacions"
    ],
    ERROR_MSG: [
        "Error message",
        "Mensaje de error",
        "Missatge d'error"
    ],
    ANY: [
        "Any",
        "Cualquiera",
        "Qualsevol"
    ],
    RESOLVED: [
        "Resolved",
        "Resuelta",
        "Resolta"
    ],
    UNRESOLVED: [
        "Unresolved",
        "No Resuelta",
        "No Resolta"
    ],
    LOCKEDF: [
        "Locked",
        "Bloqueada",
        "Bloquejada"
    ],
    RESET_SEARCH: [
        "Reset search",
        "Limpia búsqueda",
        "Neteja cerca"
    ],
    DOFILTER: [
        "Filter",
        "Filtrar",
        "Filtrar"
    ],
    DOSORT: [
        "Sort",
        "Ordenar",
        "Ordenar"
    ],
    SHOWING: [
        "Showing {{showing}} out of {{total}}",
        "Mostrando {{showing}} de un total de {{total}}",
        "Mostrant {{showing}} d'un total de {{total}}"
    ],
    SHOWING2: [
        "Total: {{total}}",
        "Total: {{total}}",
        "Total: {{total}}"
    ],
    ADD: [
        "Add",
        "Añadir",
        "Afegir"
    ],
    NEWF: [
        "New",
        "Nueva",
        "Nova"
    ],
    DELETE: [
        "Delete",
        "Eliminar",
        "Eliminar"
    ],
    PREVIEW: [
        "Preview",
        "Previsualizar",
        "Previsualitzar"
    ],
    VIEW: [
        "View",
        "Ver",
        "Veure"
    ],
    YES: [
        "Yes",
        "Sí",
        "Sí"
    ],
    NO: [
        "No",
        "No",
        "No"
    ],
    DIRTYF: [
        "Modified",
        "Modificada",
        "Modificada",
    ],
    DIRTYM: [
        "Modified",
        "Modificado",
        "Modificat",
    ],
    NOTDIRTYF: [
        "Not modified",
        "No modificada",
        "No modificada",
    ],
    NOTDIRTYM: [
        "Not modified",
        "No modificado",
        "No modificat",
    ],
    SAVE: [
        "Save",
        "Guardar",
        "Desar",
    ],
    CANCEL: [
        "Cancel",
        "Cancelar",
        "Cancel·lar",
    ],
    LOCK: [
        "Lock",
        "Bloquear",
        "Bloquejar",
    ],
    UNLOCK: [
        "Unlock",
        "Desbloquear",
        "Desbloquejar",
    ],
    COPY: [
        "Copy",
        "Copiar",
        "Copiar",
    ],
    CONFIRM_REVERT: [
        "Are you sure you want to revert the changes?",
        "¿Seguro que quieres cancelar los cambios?",
        "Segur que vols cancel·lar els canvis?",
    ],
    CONFIRM_SAVE: [
        "Are you sure you want to save the changes?",
        "¿Seguro que quieres guardar los cambios?",
        "Segur que vols desar els canvis?",
    ],
    CONFIRM_REMOVE: [
        "Are you sure you want to remove this entity? You will not be able to recover it.",
        "¿Seguro que quieres eliminar esta entidad? No podrás recuperarla.",
        "Segur que vols eliminar aquesta entitat? No podràs recuperar-la.",
    ],
    CONFIRM_COPY: [
        "Are you sure you want to copy this entity?",
        "¿Seguro que quieres copiar esta entidad?",
        "Segur que vols copiar aquesta entitat?",
    ],
    CONFIRM_LOCK: [
        "Are you sure you want to lock this entity?",
        "¿Seguro que quieres bloquear esta entidad?",
        "Segur que vols bloquejar aquesta entitat?",
    ],
    CONFIRM_UNLOCK: [
        "Are you sure you want to unlock this entity?",
        "¿Seguro que quieres desbloquear esta entidad?",
        "Segur que vols desbloquejar aquesta entitat?",
    ],
    SAVE_SUCCESS: [
        "Changes have been saved successfully.",
        "Se han guardado los cambios con éxito.",
        "S'han desat els canvis amb èxit.",
    ],
    SAVE_VALIDATION_ERROR: [
        "The following error has been detected while trying to save the changes:",
        "Se ha detectado el siguiente error al tratar de guardar los cambios:",
        "S'ha detectat el següent error a l'hora de desar els canvis:",
    ],
    REMOVED_ENTITY: [
        "The entity has been removed successfully.",
        "La entidad ha sido eliminada con éxito.",
        "L'entitat s'ha eliminat amb èxit.",
    ],
    COPY_SUCCESS: [
        "The entity has been copied successfully.",
        "La entidad ha sido copiada con éxito.",
        "L'entitat s'ha copiat amb èxit.",
    ],
    LOCK_SUCCESS: [
        "The entity has been locked successfully.",
        "La entidad ha sido bloqueada con éxito.",
        "L'entitat s'ha bloquejada amb èxit.",
    ],
    UNLOCK_SUCCESS: [
        "The entity has been unlocked successfully.",
        "La entidad ha sido desbloqueada con éxito.",
        "L'entitat s'ha desbloquejada amb èxit.",
    ],
    CHANGED_QUESTION: [
        "Question data has changed",
        "Los datos de la cuestión han cambiado",
        "Les dades de la qüestió han canviat",
    ],
    CHANGED_TESTSETS: [
        "Test Sets have changed ({{summary}})",
        "Los juegos de pruebas han cambiado ({{summary}})",
        "Els jocs de proves han canviat ({{summary}})",
    ],
    CHANGED_CHECKS: [
        "Checks have changed ({{summary}})",
        "Las comprobaciones han cambiado ({{summary}})",
        "Les comprovaciones han canviat ({{summary}})",
    ],
    VALIDATION_EMPTY: [
        "The field can not be left empty.",
        "El campo no se puede dejar vació.",
        "El camp no es pot deixar buit.",
    ],
    VALIDATION_NOTVALID: [
        "The value is not valid.",
        "El valor no es válido.",
        "El valor no és vàlid.",
    ],
    VALIDATION_NOTVALIDMUSTINTEGER: [
        "The value should be an integer.",
        "El valor debe ser un entero.",
        "El valor ha de ser un enter.",
    ],
    VALIDATION_EXISTING: [
        "Another register already exists with this same value.",
        "Ya existe otro registro con este mismo valor.",
        "Ja existeix un altre registre amb aquest mateix valor.",
    ],
    VALIDATION_POSTINITSJP: [
        "Value can't be greater than the number of Test Sets ({{tss}})",
        "El valor no puede ser mayor que el número de Juegos de pruebas ({{tss}})",
        "El valor no pot ser més gran que el número de Jocs de proves ({{tss}})",
    ],
    XNEWM: [
        "{{x}} new",
        "{{x}} nuevos",
        "{{x}} nous",
    ],
    XMODIFIEDM: [
        "{{x}} modified",
        "{{x}} modificados",
        "{{x}} modificats",
    ],
    XREMOVEDM: [
        "{{x}} removed",
        "{{x}} eliminados",
        "{{x}} eliminats",
    ],
    XNEWF: [
        "{{x}} new",
        "{{x}} nuevas",
        "{{x}} noves",
    ],
    XMODIFIEDF: [
        "{{x}} modified",
        "{{x}} modificadas",
        "{{x}} modificades",
    ],
    XREMOVEDF: [
        "{{x}} removed",
        "{{x}} eliminadas",
        "{{x}} eliminades",
    ],
    SESSION_EXPIRED: [
        "Session has expired, you'll need to log in again.",
        "La sesión ha caducado. Debes identificarte de nuevo.",
        "La sesió ha caducat. Has d'autenticar-te altre cop.",
    ],
    SIGNOUT: [
        "Sign out",
        "Salir",
        "Sortir",
    ],
    SUM: [
        "Sum",
        "Suma",
        "Suma",
    ],
    CORRECTORS: [
        "Correctors",
        "Correctores",
        "Correctors",
    ],
};

appModule.constant("translations", translations);

})();