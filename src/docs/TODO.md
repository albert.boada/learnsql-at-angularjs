#@TODO

##DataRegistry Refactor

###Current Flaws:

####Flaw #1 (SOLVED)

If first thing we do is retrieving independent instances via the `getInstance` call, and then we call `getAll` method, `getAll` will return **ONLY** the previously retrieved (cached) instances, not all records on the serverside.

#####Possibru Solushen

Populating the cached list when calling both `getAll` and `getInstance` indistinctly is such a bad idea, since we never know if cached data is all records, or just some of them. We can fix this this way:

- `getAll`'s default behaviour is **retrieving all records from the server-side if they were not cached before** with a previous `getAll` call.
- `getInstance` **will use `getAll` first** if cached records are not present, and then return the requested instance. In case cached records do exist (we can asure they are all records since we only populate the cache with the `getAll` method now), it will just return the requested instance.

######Enhancements

- `getAll` introduces a **`force-servertrip` flag** so it retrieves all records from the server, even if they were already cached in a previous call.

####Flaw #2

When modifying an entity (but not commiting the changes), original instances can NOT be touched, they have to remaing with their original values.

####Flaw #3 (SOLVED)

Derivated from Flaw #1's solushen. If we `getInstance` before `getAll`'s async work (trip to the server) has finished, we may find data-binding inconsistencies in the app.

`getAll` should have finished when returning `getInstance` requests results.

#####Possibru Solushen

Implement semaphore on `getAll`.

##Scrolling on navigate

###Current Flaws:

####Flaw #1

As it is implemented right now, there is no way to choose on runtime if we want a transition to scroll to to or not.
