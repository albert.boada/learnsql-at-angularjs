###Angular Core
-  $anchorScroll's autoScrolling watch feature **is calling NON-DECORATED scroll() method**, instead of the decorated.
-  Clicking on a manually-constructed anchor href (/foo#bottom), while being in the very same anchor url (/foo#bottom), won't scroll you to the anchor again (because $location does not get updated, since it is the same). WOULD BE NICE.

###Angular-Ui-Route
- No ui-views are updated when clicking an anchor link (/foo#bottom) for the same current state (/foo). Since no STATE is changed, $uiViewScroll() is NEVER triggered, so we are never scrolled to the anchor. Here is where autoScrolling-$watch would do a nice job, but since it is not very consistent, we don't want to rely on it.
  Surprisingly enough, ngRoute does not behave like this. Even if we are in the same current state (/foo), clicking on the anchor (/foo#bottom) will scroll us!! Where is where Ui-Route fails???
BUT, if we are in other state (/oof) and click into a new state with an anchor (/foo#bottom), we get transitioned and scrolled to anchor

|          | /foo#bottom -> /foo#bottom | /foo -> /foo#bottom | /foo -> /oof#bottom |
|----------|------------------------------|-----------|-------------------|
| **ng**Route  | **NO** scroll (can be solved)                 | **SCROLL**    | **SCROLL** |
| **ui**Route  | **NO** scroll                    | **NO** scroll | **SCROLL** |

